package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.NTriplesEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.data.API_Data_CSVDatasetEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.meta.API_Meta_Country_CSVDatasetEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.meta.API_Meta_Indicator_CSVDatasetEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.defwdi.data.DEFWDI_Data_CSVDatasetEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.defwdi.meta.DEFWDI_Meta_CSVDatasetEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.ersst.v4.data.ERSST_V4_Month_Data_CSVDatasetEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.ersst.v4.data.ERSST_V4_Year_Data_CSVDatasetEngine;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by nyx on 6/3/17.
 */
public class Main {

    public static void main(String[] args) {

        // Time Execution
        long startTime, endTime;
        if (Constants.TIME_DEBUG || Constants.DEBUG) startTime = System.currentTimeMillis();

        Model model = null;

        if (Constants.TRANSLATION_PROCESS) {

            // Create a new Semantic Model for Everything
            SemanticModel semanticModel = new SemanticModel();

            if (Constants.PROCESS_API) {
                if (Constants.PROCESS_API_DATA) {
                    // Read API_DATA.CSV and create a SemanticModel
                    SemanticModel api_data_model = (new API_Data_CSVDatasetEngine()).readDataset(); //StudentsCSVEngine.readCSV(Constants.STUDENTS_CSV_IN_PATH);
                    // Add it to the model
                    semanticModel = semanticModel.concat(api_data_model);
                }

                // Read API_COUNTRY.CSV and create a SemanticModel
                SemanticModel api_meta_country_model = (new API_Meta_Country_CSVDatasetEngine()).readDataset(); //StudentsCSVEngine.readCSV(Constants.STUDENTS_CSV_IN_PATH);
                // Add it to the model
                semanticModel = semanticModel.concat(api_meta_country_model);

                // Read API_INDICATOR.CSV and create a SemanticModel
                SemanticModel api_meta_indicator_model = (new API_Meta_Indicator_CSVDatasetEngine()).readDataset(); //StudentsCSVEngine.readCSV(Constants.STUDENTS_CSV_IN_PATH);
                // Add it to the model
                semanticModel = semanticModel.concat(api_meta_indicator_model);

                // Build Single Model
                //SemanticModel semanticModel = api_data_model.concat(api_meta_country_model);
                //semanticModel = semanticModel.concat(api_meta_indicator_model);
                //SemanticModel semanticModel = api_meta_country_model;
                //SemanticModel semanticModel = api_meta_indicator_model;
                //SemanticModel semanticModel = api_meta_country_model.concat(api_meta_indicator_model);
            }

            if (Constants.PROCESS_DEFWDI) {
                if (Constants.PROCESS_DEFWDI_DATA) {
                    // Read DEFWDI_DATA.CSV and create a SemanticModel
                    SemanticModel defwdi_data_model = (new DEFWDI_Data_CSVDatasetEngine()).readDataset();
                    // Add it to the model
                    semanticModel = semanticModel.concat(defwdi_data_model);
                    // Read DEFWDI_META.CSV and create a SemanticModel
                    SemanticModel defwdi_meta_model = (new DEFWDI_Meta_CSVDatasetEngine()).readDataset();
                    // Add it to the model
                    semanticModel = semanticModel.concat(defwdi_meta_model);
                }
            }

            if (Constants.PROCESS_ERSST) {
                if (Constants.PROCESS_ERSST_V4) {
                    if (Constants.PROCESS_ERSST_V4_MON) {
                        // Read ERSST_MON.CSV and create a SemanticModel
                        SemanticModel ersst_mon = (new ERSST_V4_Month_Data_CSVDatasetEngine()).readDataset();
                        // Add it to the model
                        semanticModel = semanticModel.concat(ersst_mon);
                    }
                    if (Constants.PROCESS_ERSST_V4_ANN) {
                        // Read ERSST_ANN.CSV and create a SemanticModel
                        SemanticModel ersst_ann = (new ERSST_V4_Year_Data_CSVDatasetEngine()).readDataset();
                        // Add it to the model
                        semanticModel = semanticModel.concat(ersst_ann);
                    }
                }
            }

            // Print N-Triples
            if (Constants.TEST) System.out.println(new NTriplesEngine().convert(semanticModel));

            // Convert Model to Jena's Format
            model = JenaTranslation.convert(semanticModel);

        } else if (Constants.UNIFICATION_PROCESS) {
            model = ModelFactory.createDefaultModel();
            for (String rdf_file_in : Constants.RDF_FILE_INPUT_GRAPH_PARTITIONS) {
                if (!rdf_file_in.equals(Constants.NULL_MODEL_RDF)) {
                    if (Constants.DEBUG) System.out.printf("%s ...", rdf_file_in);
                    model.read(rdf_file_in);
                    if (Constants.DEBUG) System.out.printf(" done\n");
                }
            }
        } else if (Constants.FORMAT_TRANSLATION_PROCESS) {
            model = ModelFactory.createDefaultModel();
            model.read(Constants.RDF_FILE_IN_PATH);
        } else if (Constants.FORMAT_TRANSLATION_MULTIPLE_PROCESS) {
            if (Constants.FORMAT_TRANSLATION_NUMBER_ENUMERATION) {

                // loop over split files
                for (byte index = Constants.RDF_MULTIPLE_FILE_INICIAL_ENUMERATION_NUMBER;
                     index <= Constants.RDF_MULTIPLE_FILE_FINAL_ENUMERATION_NUMBER; index++) {

                    // generate expected file path + name
                    String in_file_path = String.format(
                            Constants.RDF_MULTIPLE_FILE_IN_PATH_PATTERN,
                            String.format(
                                    "%0" +
                                            Constants.RDF_MULTIPLE_FILE_NUMBER_OF_ENUMERATION_DIGITS +
                                            "d",
                                    index
                            )
                    );

                    // create temporary Jena model for the current file
                    Model temp_model = ModelFactory.createDefaultModel();

                    // display file path + name to read
                    if (Constants.DEBUG) System.out.printf("Reading... %s\n", in_file_path);

                    // read in file
                    temp_model.read(in_file_path, Constants.RDF_INPUT_FILE_TYPE.getJenaType());

                    // Print Jena's Model
                    if (Constants.TEST) System.out.println(JenaTranslation.asStatements(temp_model));

                    // Print Jena's Model as RDF/XML
                    if (Constants.TEST) temp_model.write(System.out, "RDF/XML-ABBREV");

                    // Add new extension to in-file and emit as out-file
                    String out_file_path = in_file_path + "." + Constants.RDF_OUTPUT_FILE_TYPE.getExt();

                    // Save Jena's Model as RDF/XML
                    File file = new File(out_file_path);
                    FileOutputStream fileOutputStream;
                    try {
                        fileOutputStream = new FileOutputStream(file);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        return;
                    }

                    // display file path + name to write
                    if (Constants.DEBUG) System.out.printf("Writing... %s\n", out_file_path);

                    RDFWriter writer = temp_model.getWriter(Constants.RDF_OUTPUT_FILE_TYPE.getJenaType()); //RDF/XML
                    writer.write(temp_model, fileOutputStream, "");
                }

            } else if (Constants.FORMAT_TRANSLATION_LETTER_ENUMERATION) {
                // TODO
            } else {
                System.err.println("Unknown enumeration type...");
            }
        } else {
            System.err.println("No operation selected...");
        }

        if (model != null) {
            // Print Jena's Model
            if (Constants.TEST) System.out.println(JenaTranslation.asStatements(model));

            // Print Jena's Model as RDF/XML
            if (Constants.TEST) model.write(System.out, "RDF/XML-ABBREV");

            // Save Jena's Model as RDF/XML
            File file = new File(Constants.RDF_FILE_OUT_PATH);
            FileOutputStream fileOutputStream;
            try {
                fileOutputStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
            RDFWriter writer = model.getWriter(Constants.RDF_OUTPUT_FILE_TYPE.getJenaType()); //RDF/XML
            writer.write(model, fileOutputStream, "");
        }

        // Measure time taken...
        if (Constants.TIME_DEBUG || Constants.DEBUG) {
            endTime = System.currentTimeMillis();
            long elapsedTime = endTime - startTime;
            double time = elapsedTime / 1000;
            int seconds = (int) time, minutes = 0, hours = 0, days = 0;
            time /= 60;
            if (time >= 1) {
                minutes = (int) time;
                seconds -= minutes * 60;
                time /= 60;
                if (time >= 1) {
                    hours = (int) time;
                    minutes -= hours * 60;
                    time /= 24;
                    if (time >= 1) {
                        days = (int) time;
                        hours -= days * 24;
                    }
                }
            }
            System.out.printf("Elapsed Time: %d day(s) %d hour(s) %d minute(s) %d second(s) \n", days, hours, minutes, seconds);
        }
    }

}
