package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.factories;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Predicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Vocabulary;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Word;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nyx on 6/3/17.
 */
public class VocabularyWordFactory {
    private Vocabulary vocabulary;
    private List<Word> words;
    private List<InstanceResource> instances;

    public VocabularyWordFactory(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
        this.words = new ArrayList<Word>();
        this.instances = new ArrayList<InstanceResource>();
    }

    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    public void setVocabulary(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public List<InstanceResource> getInstances() {
        return instances;
    }

    public void setInstances(List<InstanceResource> instances) {
        this.instances = instances;
    }

    public Word createWord(String word, Constants.WordType type) throws InvalidWordTypeException {
        Word newWord = null;
        if (type == Constants.WordType.RESOURCE) {
            newWord = new ResourceObject(this.vocabulary, word);
        } else if (type == Constants.WordType.PREDICATE) {
            newWord = new Predicate(this.vocabulary, word);
        } else {
            throw new InvalidWordTypeException();
        }
        if (!words.contains(newWord)) words.add(newWord);
        return newWord;
    }

    public void addWord(String word, Constants.WordType type) throws InvalidWordTypeException {
        createWord(word, type);
    }

    public void addWord(Word word) {
        if (!words.contains(word)) words.add(word);
    }

    public InstanceResource createInstance(String word, ResourceObject type) {
        InstanceResource newInstance = new InstanceResource(vocabulary, word, type);
        if (!instances.contains(newInstance)) instances.add(newInstance);
        return newInstance;
    }

    public void addInstance(String word, ResourceObject type) {
        createInstance(word, type);
    }

    public void addInstance(InstanceResource instance) {
        if (!instances.contains(instance)) instances.add(instance);
    }
}
