package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.meta;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic.StringProc;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.API_CSVDatasetEngine;

import java.util.List;
import java.util.Scanner;

/**
 * Created by nyx on 6/11/17.
 */
public class API_Meta_Country_CSVDatasetEngine extends API_CSVDatasetEngine {

    public API_Meta_Country_CSVDatasetEngine() {
        super(Constants.API_META_COUNTRY_DATASET_NAME, Constants.API_META_COUNTRY_DATASET_CSV_PATH,
                Constants.API_META_COUNTRY_DATASET_NUMBER_HEADER_LINES);
    }

    public SemanticModel readDataset() {
        SemanticModel model = new SemanticModel();

        List<String> rows = this.getInput();

        if (!Constants.isInicialized()) try {
            Constants.inicialize();
        } catch (InvalidWordTypeException e) {
            e.printStackTrace();
            return null;
        }

        int row_num = 2;

        int total_rows = rows.size();

        for (String row : rows) {

            if (Constants.DEBUG) System.out.printf("API_COUNTRY.CSV [ %05d / %05d ]:", row_num, total_rows);

            Scanner rowReader = new Scanner(row);
            rowReader.useDelimiter(Constants.API_META_COUNTRY_DATASET_DELIMITER);

            String countryName = rowReader.next();
            countryName = countryName.replace("\"","");
            if (!countryName.equals("NULL")) {
                InstanceResource country = new InstanceResource(Constants.CLIM_DATA, countryName, Constants.COUNTRY_TYPE);
                if (Constants.DEBUG) System.out.print(" C");

                String regionName = rowReader.next();
                regionName = regionName.replace("\"", "");
                if (!regionName.equals("NULL")) {
                    String regionAbrv = StringProc.abreviate(regionName);
                    InstanceResource region = new InstanceResource(Constants.CLIM_DATA, "Region_" + regionAbrv, Constants.REGION_TYPE);
                    region.addPredicateObject(new PredicateLiteralObject(Constants.DCT_title, regionName));
                    model.addResource(region);
                    country.addPredicateObject(new PredicateResourceObject(Constants.CLIM_region, region));
                    if (Constants.DEBUG) System.out.print(" R");
                }

                String incomeGroupName = rowReader.next();
                if (!incomeGroupName.equals("NULL")) {
                    country.addPredicateObject(new PredicateLiteralObject(Constants.DCT_description, "Income Group: " + incomeGroupName));
                    if (Constants.DEBUG) System.out.print(" I");
                }

                String specialNotes = rowReader.next();
                specialNotes = specialNotes.replace("\"", "");
                if (!specialNotes.equals("NULL")) {
                    country.addPredicateObject(new PredicateLiteralObject(Constants.DCT_description, specialNotes));
                    if (Constants.DEBUG) System.out.print(" S");
                }

                String countryFullName = rowReader.next();
                countryFullName = countryFullName.replace("\"", "");
                if (!countryFullName.equals("NULL")) {
                    country.addPredicateObject(new PredicateLiteralObject(Constants.DCT_title, countryFullName));
                    if (Constants.DEBUG) System.out.print(" N");
                }

                model.addResource(country);
            }

            if (Constants.DEBUG) {
                row_num++;
                System.out.println();
            }
        }

        return model;
    }


}
