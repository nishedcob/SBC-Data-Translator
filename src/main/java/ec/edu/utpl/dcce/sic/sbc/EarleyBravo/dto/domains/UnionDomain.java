package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.domains;


import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Domain;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Word;

import java.util.List;

/**
 * Created by nyx on 6/3/17.
 */
public class UnionDomain implements Domain {
    private List<Domain> domains;

    public UnionDomain(List<Domain> domains) {
        this.domains = domains;
    }

    public List<Domain> getDomains() {
        return domains;
    }

    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }

    public void addDomain(Domain domain) {
        if (!domains.contains(domain)) domains.add(domain);
    }

    public void removeDomain(Domain domain) {
        if (domains.contains(domain)) domains.remove(domain);
    }

    public boolean validate(Word word) {
        for (Domain d : domains) {
            if (d.validate(word)) return true;
        }
        return false;
    }
}
