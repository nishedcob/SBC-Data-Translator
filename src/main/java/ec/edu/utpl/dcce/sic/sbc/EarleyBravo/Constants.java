package ec.edu.utpl.dcce.sic.sbc.EarleyBravo;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Predicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Vocabulary;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.factories.VocabularyWordFactory;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicates.DataPropertyPredicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicates.RelationshipPredicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.ResourceSubclass;

/**
 * Created by nyx on 6/3/17.
 */
public class Constants {

    // General Flags
    public final static boolean DEBUG = true;
    public final static boolean TIME_DEBUG = true;
    public final static boolean TEST = false;
    // Operation Flags
    public final static boolean TRANSLATION_PROCESS = false;
    public final static boolean UNIFICATION_PROCESS = false;
    public final static boolean FORMAT_TRANSLATION_PROCESS = false;
    public final static boolean FORMAT_TRANSLATION_MULTIPLE_PROCESS = true;
    // API Dataset Flags
    public static final boolean PROCESS_API = false;
    public static final boolean PROCESS_API_DATA = false;
    // DEFWDI Dataset Flags
    public static final boolean PROCESS_DEFWDI = false;
    public static final boolean PROCESS_DEFWDI_DATA = false;
    // ERSST Dataset Flags
    public static final boolean PROCESS_ERSST = false;
    public static final boolean PROCESS_ERSST_V4 = false;
    public static final boolean PROCESS_ERSST_V4_MON = false;
    public static final boolean PROCESS_ERSST_V4_ANN = false;
    // Unification Flags
    public static final boolean UNIFY_API = false;
    public static final boolean UNIFY_DEFWDI = false;
    public static final boolean UNIFY_ERSST_ANN = false;
    public static final boolean UNIFY_ERSST_MON = false;
    // Format Translation Flags
    public static final boolean FORMAT_TRANSLATION_IN_RDF = false;
    public static final boolean FORMAT_TRANSLATION_IN_NT = true;
    public static final boolean FORMAT_TRANSLATION_OUT_RDF = true;
    public static final boolean FORMAT_TRANSLATION_OUT_NT = false;
    // Format Translation Multiple Flags
    public static final boolean FORMAT_TRANSLATION_NUMBER_ENUMERATION = true;
    public static final boolean FORMAT_TRANSLATION_LETTER_ENUMERATION = false;
    public static final boolean FORMAT_TRANSLATION_CAPTIAL_LETTER_ENUMERATION = false;

    public final static String YEAR_FORMAT = "Year_%d";
    public final static String STATION_FORMAT = "Station_%s";
    public final static String DUAL_LAT_FORMAT = "%s_%s";
    public final static String LOCATION_FORMAT = "Location_%s";
    public final static String DATE_FORMAT = "Date_%s";
    public final static String MONTH_YEAR_FORMAT = "%s_%s_(Month)";
    public final static String DATA_FORMAT = "Data_%s";
    public final static String DATA_TRIPLE_IDENTIFIER_FORMAT = "%s_%s_%s";
    public final static String DATA_DOUBLE_IDENTIFIER_FORMAT = "%s_%s";

    public final static String[] MONTHS_OF_THE_YEAR = {
            "January", "Feburary", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
    };

    public final static int MIN_MONTH_NUMBER = 0;
    public final static int MAX_MONTH_NUMBER = MONTHS_OF_THE_YEAR.length - 1;

    public final static String UNKNOWN = "Unknown";
    public final static String UNKNOWN_STATION = UNKNOWN;
    public final static int NON_EXISTENT_MONTH = -13;

    public enum JenaFileType {
        RDF_XML("rdf", "RDF/XML"), N_TRIPLES("nt", "N-TRIPLES");
        private String ext;
        private String jenaType;

        JenaFileType(String ext, String jenaType) {
            this.ext = ext;
            this.jenaType = jenaType;
        }

        public String getExt() {
            return ext;
        }

        public String getJenaType() {
            return jenaType;
        }
    }

    public final static JenaFileType RDF_INPUT_FILE_TYPE = (
            FORMAT_TRANSLATION_IN_RDF ?
                    JenaFileType.RDF_XML :
                    (
                            FORMAT_TRANSLATION_IN_NT ?
                                    JenaFileType.N_TRIPLES :
                                    null
                    )
    );
    public final static String DIRECTORY_FILE_IN_PATH = "/tmp";
    public final static String RDF_FILE_IN_PATH = DIRECTORY_FILE_IN_PATH + "/full_model." + RDF_INPUT_FILE_TYPE.getExt();
    public final static String RDF_MULTIPLE_FILE_IN_PATH_PATTERN = DIRECTORY_FILE_IN_PATH + "/full_model." + RDF_INPUT_FILE_TYPE.getExt() + ".%s";
    public final static char RDF_MULTIPLE_FILE_INICIAL_ENUMERATION_LETTER = (FORMAT_TRANSLATION_CAPTIAL_LETTER_ENUMERATION ? 'A' : 'a');
    public final static char RDF_MULTIPLE_FILE_FINAL_ENUMERATION_LETTER = (FORMAT_TRANSLATION_CAPTIAL_LETTER_ENUMERATION ? 'Z' : 'z');
    public final static byte RDF_MULTIPLE_FILE_NUMBER_OF_ENUMERATION_DIGITS = 2;
    public final static byte RDF_MULTIPLE_FILE_INICIAL_ENUMERATION_NUMBER = 0;
    public final static byte RDF_MULTIPLE_FILE_FINAL_ENUMERATION_NUMBER = 10;

    public final static JenaFileType RDF_OUTPUT_FILE_TYPE = (
            FORMAT_TRANSLATION_OUT_RDF ?
                    JenaFileType.RDF_XML :
                    (
                            FORMAT_TRANSLATION_OUT_NT ?
                                    JenaFileType.N_TRIPLES :
                                    null
                    )
    );
    public final static String DIRECTORY_FILE_OUT_PATH = "/tmp";
    public final static String RDF_FILE_OUT_PATH = DIRECTORY_FILE_OUT_PATH + "/full_model." + RDF_OUTPUT_FILE_TYPE.getExt();

    public static final String NULL_MODEL_RDF = "NULL";
    public static final String API_FULL_MODEL_RDF = DIRECTORY_FILE_IN_PATH + "/full_model_api.rdf";
    public static final String DEFWDI_FULL_MODEL_RDF = DIRECTORY_FILE_IN_PATH + "/full_model_defwdi.rdf";
    public static final String ERSST_ANN_FULL_MODEL_RDF = DIRECTORY_FILE_IN_PATH + "/full_model_ersst_ann.rdf";
    public static final String ERSST_MON_FULL_MODEL_RDF = DIRECTORY_FILE_IN_PATH + "/full_model_ersst_mon.rdf";

    public static final String[] RDF_FILE_INPUT_GRAPH_PARTITIONS = {
            ( UNIFY_API ? API_FULL_MODEL_RDF : NULL_MODEL_RDF ),
            ( UNIFY_DEFWDI ? DEFWDI_FULL_MODEL_RDF : NULL_MODEL_RDF ),
            ( UNIFY_ERSST_ANN ? ERSST_ANN_FULL_MODEL_RDF : NULL_MODEL_RDF ),
            ( UNIFY_ERSST_MON ? ERSST_MON_FULL_MODEL_RDF : NULL_MODEL_RDF )
    };

    public final static String CSV_ENGINE_DATASET_SUFIX = "_CSV";

    public final static String API_DATASET_PREFIX = "API_";
    public final static String API_URL = "http://api.worldbank.org/v2/en/topic/19?downloadformat=csv";
    public final static String API_DATA_DIRECTORY = DIRECTORY_FILE_IN_PATH + "/api";
    public final static String API_META_DIRECTORY = API_DATA_DIRECTORY;

    public final static String API_DATA_DATASET_NAME = "DATA";
    public final static String API_DATA_DATASET_CSV_PATH = API_DATA_DIRECTORY + "/api_data.csv";
    public final static int API_DATA_DATASET_NUMBER_HEADER_LINES = 1;
    public final static String API_DATA_DATASET_DELIMITER = ",";
    public final static int API_DATA_DATASET_MIN_YR = 1960;
    public final static int API_DATA_DATASET_MAX_YR = 2016;

    public final static String API_META_DATASET_NAME = "META";
    public final static int API_META_DATASET_NUMBER_HEADER_LINES = 1;
    public final static String API_META_DATASET_DELIMITER = ",";

    public final static String API_META_COUNTRY_DATASET_NAME = API_META_DATASET_NAME + "_COUNTRY";
    public final static String API_META_COUNTRY_DATASET_CSV_PATH = API_META_DIRECTORY + "/api_country.csv";
    public final static int API_META_COUNTRY_DATASET_NUMBER_HEADER_LINES = API_META_DATASET_NUMBER_HEADER_LINES;
    public final static String API_META_COUNTRY_DATASET_DELIMITER = API_META_DATASET_DELIMITER;

    public final static String API_META_INDICATOR_DATASET_NAME = API_META_DATASET_NAME + "_INDICATOR";
    public final static String API_META_INDICATOR_DATASET_CSV_PATH = API_META_DIRECTORY + "/api_indicator.csv";
    public final static int API_META_INDICATOR_DATASET_NUMBER_HEADER_LINES = API_META_DATASET_NUMBER_HEADER_LINES;
    public final static String API_META_INDICATOR_DATASET_DELIMITER = API_META_DATASET_DELIMITER;

    public final static String DEFWDI_DATASET_PREFIX = "DEFWDI_";
    public final static String DEFWDI_URL = "";
    public final static String DEFWDI_DATA_DIRECTORY = "/tmp/defwdi";
    public final static String DEFWDI_META_DIRECTORY = DEFWDI_DATA_DIRECTORY;

    public final static String DEFWDI_DATA_DATASET_NAME = "DATA";
    public final static String DEFWDI_META_DATASET_NAME = "META";
    public final static String DEFWDI_DATA_DATASET_CSV_PATH = DEFWDI_DATA_DIRECTORY + "/defwdi_data.csv";
    public final static String DEFWDI_META_DATASET_CSV_PATH = DEFWDI_META_DIRECTORY + "/defwdi_meta.csv";
    public final static int DEFWDI_DATA_DATASET_NUMBER_HEADER_LINES = 1;
    public final static int DEFWDI_META_DATASET_NUMBER_HEADER_LINES = 1;
    public final static String DEFWDI_DATA_DATASET_DELIMITER = ",";
    public final static String DEFWDI_META_DATASET_DELIMITER = ",";
    public final static int DEFWDI_DATA_DATASET_MIN_YR = 2007;
    public final static int DEFWDI_DATA_DATASET_MAX_YR = 2016;
    public final static String DEFWDI_NULL_VALUE = "NULL";
    public final static String DEFWDI_ALTERNATE_NULL_VALUE = "..";
    public final static String DEFWDI_DATA_NULL_VALUE = DEFWDI_NULL_VALUE;
    public final static String DEFWDI_DATA_ALTERNATE_NULL_VALUE = DEFWDI_ALTERNATE_NULL_VALUE;
    public final static String DEFWDI_META_NULL_VALUE = DEFWDI_NULL_VALUE;
    public final static String DEFWDI_META_ALTERNATE_NULL_VALUE = DEFWDI_ALTERNATE_NULL_VALUE;

    public final static String ERSST_DATASET_PREFIX = "ERSST_";
    public final static String ERSST_V4_DATASET_PREFIX = ERSST_DATASET_PREFIX + "V4_";
    public final static String ERSST_URL = "";
    public final static String ERSST_DATA_DIRECTORY = "/tmp/ersst";
    public final static String ERSST_V4_DATA_DIRECTORY = ERSST_DATA_DIRECTORY + "/v4";

    public final static String ERSST_DATA_DATASET_NAME = "DATA";
    public final static String ERSST_MONTH_DATA_DATASET_NAME = "MON_" + ERSST_DATA_DATASET_NAME;
    public final static String ERSST_YEAR_DATA_DATASET_NAME = "ANN_" + ERSST_DATA_DATASET_NAME;
    public final static String ERSST_V4_MONTH_DATA_DATASET_CSV_PATH = ERSST_V4_DATA_DIRECTORY + "/united-mon.csv";
    public final static String ERSST_V4_YEAR_DATA_DATASET_CSV_PATH = ERSST_V4_DATA_DIRECTORY + "/united-ann.csv";
    public final static int ERSST_V4_MONTH_DATA_DATASET_NUMBER_DATAPOINTS = 7;
    public final static int ERSST_V4_YEAR_DATA_DATASET_NUMBER_DATAPOINTS = 5;
    public final static String ERSST_V4_DATA_READING_TYPE_LAND = "land";
    public final static String ERSST_V4_DATA_READING_TYPE_OCEAN = "ocean";
    public final static String ERSST_V4_DATA_READING_TYPE_LAND_OCEAN = "land_ocean";
    public final static String[] ERSST_V4_DATA_READING_TYPE = {
            ERSST_V4_DATA_READING_TYPE_LAND, ERSST_V4_DATA_READING_TYPE_OCEAN, ERSST_V4_DATA_READING_TYPE_LAND_OCEAN
    };
    public final static int ERSST_DATA_DATASET_NUMBER_HEADER_LINES = 0;
    public final static String ERSST_DATA_DATASET_DELIMITER = ",";
    public final static String ERSST_NULL_VALUE = "NULL";
    public final static String ERSST_ALTERNATE_NULL_VALUE = "..";
    public final static String ERSST_DATA_NULL_VALUE = ERSST_NULL_VALUE;
    public final static String ERSST_DATA_ALTERNATE_NULL_VALUE = ERSST_ALTERNATE_NULL_VALUE;
    public final static String ERSST_V4_DATA_NULL_VALUE = ERSST_DATA_NULL_VALUE;
    public final static String ERSST_V4_DATA_ALTERNATE_NULL_VALUE = ERSST_DATA_ALTERNATE_NULL_VALUE;

    public enum WordType {
        RESOURCE, PREDICATE
    }

    public enum ResourceType {
        CLASS, INSTANCE
    }

    public enum PredicateType {
        RELATIONSHIP, DATA_PROPERTY
    }

    public enum ObjectType {
        RESOURCE, LITERAL
    }

    public enum LiteralDataType {
        STRING, INTEGER, DOUBLE
    }

    public static String INVALID_WORD_TYPE_ERROR_MESSAGE = "Error: Tipo de Palabra Disconocido!";

    private static boolean inicialize_constants = false;

    public static boolean isInicialized() {
        return inicialize_constants;
    }

    public static Vocabulary RDFS = new Vocabulary("rdfs", "https://www.w3.org/2000/01/RDF-schema#");
    public static VocabularyWordFactory RDFS_FACTORY = new VocabularyWordFactory(RDFS);
    public static ResourceObject RDFS_CLASS;

    public static Vocabulary RDF = new Vocabulary("rdf", "http://www.w3.org/1999/02/22-RDF-syntax-ns#");
    public static VocabularyWordFactory RDF_FACTORY = new VocabularyWordFactory(RDF);
    public static Predicate RDF_type = null;

    public static Vocabulary OWL = new Vocabulary("owl", "http://www.w3.org/2002/07/owl#");
    public static VocabularyWordFactory OWL_FACTORY = new VocabularyWordFactory(OWL);
    public static Predicate OWL_ObjectProperty = null;
    public static ResourceObject OWL_Class = null;

    public static Vocabulary DCT = new Vocabulary("dc", "http://purl.org/dc/terms/");
    public static VocabularyWordFactory DCT_FACTORY = new VocabularyWordFactory(DCT);
    public static Predicate DCT_title = null, DCT_description = null, DCT_source = null;

    public static Vocabulary CLIM = new Vocabulary("clim", "http://climate.utpl.edu.ec/vocab/");
    public static VocabularyWordFactory CLIM_FACTORY = new VocabularyWordFactory(CLIM);

    public static Predicate CLIM_areaClassification = null, CLIM_areaClassificationBySatilite = null,
            CLIM_areaVegetationType = null, CLIM_belongsToDataSeries = null, CLIM_indicator_Type, CLIM_locatedIn = null,
            CLIM_measurementDate = null, CLIM_month = null, CLIM_region = null, CLIM_takenAt = null, CLIM_topologyType = null,
            CLIM_year = null,CLIM_country=null;

    public static Predicate CLIM_catagoryName = null,CLIM_dataPoint=null,CLIM_dayOfTheMonth=null,
            CLIM_dayOfTheweek=null,CLIM_dayOfTheYear=null,CLIM_identifier=null,CLIM_monthOfTheYear=null,CLIM_name=null;

    public static ResourceObject CLIM_AreaType = null, CLIM_Station = null, CLIM_AreaVegetationType = null,
            CLIM_VegetationType = null, CLIM_Data = null, CLIM_DataSeries, CLIM_Country = null, CLIM_IndicatorData = null,
            CLIM_Location = null, CLIM_Date = null, CLIM_Measurement = null, CLIM_Month = null, CLIM_CountryRegion = null,
            CLIM_TopographyType = null, CLIM_AirportStation = null, CLIM_Indicator = null, CLIM_City = null,
            CLIM_LandTemperatureMeasurement = null, CLIM_TemperatureMeasurement = null, CLIM_OceanTemperatureMeasurement = null,
            CLIM_MaxTemperatureMeasurement = null, CLIM_MinTemperatureMeasurement = null,
            CLIM_PreipitationMeasurement = null, CLIM_RainfallMeasurement = null, CLIM_SnowfallMeasurement,
            CLIM_StateRegion = null, CLIM_TimeTemperatureMeasurement = null,CLIM_Year=null;

    public static Vocabulary CLIM_DATA= new Vocabulary("clim_data", "http://climate.utpl.edu.ec/data/");
    public static VocabularyWordFactory CLIM_Data_FACTORY = new VocabularyWordFactory(CLIM_DATA);

    public static void inicialize() throws InvalidWordTypeException {
        if (inicialize_constants) return;
        RDFS_CLASS = (ResourceObject) RDFS_FACTORY.createWord("Class", WordType.RESOURCE);
        RDF_type = (Predicate) RDF_FACTORY.createWord("type", Constants.WordType.PREDICATE);

        DCT_title = (Predicate) DCT_FACTORY.createWord("title", WordType.PREDICATE);
        DCT_description = (Predicate) DCT_FACTORY.createWord("description", WordType.PREDICATE);
        DCT_source = (Predicate) DCT_FACTORY.createWord("source", WordType.PREDICATE);

        CLIM_areaClassification = (Predicate) CLIM_FACTORY.createWord("areaClassification", WordType.PREDICATE);
        CLIM_areaClassificationBySatilite= (Predicate) CLIM_FACTORY.createWord("areaClassificationBySatilite", WordType.PREDICATE);
        CLIM_areaVegetationType  = (Predicate) CLIM_FACTORY.createWord("areaVegetationType", WordType.PREDICATE);
        CLIM_belongsToDataSeries  = (Predicate) CLIM_FACTORY.createWord("belongsToDataSeries", WordType.PREDICATE);
        CLIM_indicator_Type = (Predicate) CLIM_FACTORY.createWord("belongsToDataSeries", WordType.PREDICATE);
        CLIM_locatedIn = (Predicate) CLIM_FACTORY.createWord("locatedIn", WordType.PREDICATE);
        CLIM_measurementDate  = (Predicate) CLIM_FACTORY.createWord("measurementDate", WordType.PREDICATE);
        CLIM_month = (Predicate) CLIM_FACTORY.createWord("month", WordType.PREDICATE);
        CLIM_region  = (Predicate) CLIM_FACTORY.createWord("region", WordType.PREDICATE);
        CLIM_takenAt = (Predicate) CLIM_FACTORY.createWord("takenAt", WordType.PREDICATE);
        CLIM_topologyType = (Predicate) CLIM_FACTORY.createWord("topologyType", WordType.PREDICATE);
        CLIM_year  = (Predicate) CLIM_FACTORY.createWord("year", WordType.PREDICATE);
        CLIM_country = (Predicate) CLIM_FACTORY.createWord("country", WordType.PREDICATE);

        CLIM_catagoryName = (Predicate) CLIM_FACTORY.createWord("catagoryName", WordType.PREDICATE);
        CLIM_dataPoint    = (Predicate) CLIM_FACTORY.createWord("dataPoint", WordType.PREDICATE);
        CLIM_dayOfTheMonth = (Predicate) CLIM_FACTORY.createWord("dayOfTheMonth", WordType.PREDICATE);
        CLIM_dayOfTheweek = (Predicate) CLIM_FACTORY.createWord("dayOfTheweek", WordType.PREDICATE);
        CLIM_dayOfTheYear = (Predicate) CLIM_FACTORY.createWord("dayOfTheYear", WordType.PREDICATE);
        CLIM_identifier = (Predicate) CLIM_FACTORY.createWord("identifier", WordType.PREDICATE);
        CLIM_monthOfTheYear = (Predicate) CLIM_FACTORY.createWord("monthOfTheYear", WordType.PREDICATE);
        CLIM_name = (Predicate) CLIM_FACTORY.createWord("name", WordType.PREDICATE);

        CLIM_AreaType = (ResourceObject) CLIM_FACTORY.createWord("AreaType", WordType.RESOURCE);
        CLIM_Station = (ResourceObject) CLIM_FACTORY.createWord("Station", WordType.RESOURCE);
        CLIM_AreaVegetationType = (ResourceObject) CLIM_FACTORY.createWord("AreaVegetationType", WordType.RESOURCE);
        CLIM_VegetationType = (ResourceObject) CLIM_FACTORY.createWord("VegetationType", WordType.RESOURCE);
        CLIM_Data = (ResourceObject) CLIM_FACTORY.createWord("Data", WordType.RESOURCE);
        CLIM_DataSeries = (ResourceObject) CLIM_FACTORY.createWord("DataSeries", WordType.RESOURCE);
        CLIM_Country = (ResourceObject) CLIM_FACTORY.createWord("Country", WordType.RESOURCE);
        CLIM_IndicatorData = (ResourceObject) CLIM_FACTORY.createWord("IndicatorData", WordType.RESOURCE);
        CLIM_Location = (ResourceObject) CLIM_FACTORY.createWord("Location", WordType.RESOURCE);
        CLIM_Date = (ResourceObject) CLIM_FACTORY.createWord("Date", WordType.RESOURCE);
        CLIM_Measurement = (ResourceObject) CLIM_FACTORY.createWord("Measurement", WordType.RESOURCE);
        CLIM_Month = (ResourceObject) CLIM_FACTORY.createWord("Month", WordType.RESOURCE);
        CLIM_CountryRegion = (ResourceObject) CLIM_FACTORY.createWord("CountryRegion", WordType.RESOURCE);
        CLIM_TopographyType = (ResourceObject) CLIM_FACTORY.createWord("TopographyType", WordType.RESOURCE);
        CLIM_AirportStation = (ResourceObject) CLIM_FACTORY.createWord("AirportStation", WordType.RESOURCE);
        CLIM_Indicator = (ResourceObject) CLIM_FACTORY.createWord("Indicator", WordType.RESOURCE);
        CLIM_City = (ResourceObject) CLIM_FACTORY.createWord("City", WordType.RESOURCE);
        CLIM_LandTemperatureMeasurement = (ResourceObject) CLIM_FACTORY.createWord("LandTemperatureMeasurement", WordType.RESOURCE);
        CLIM_TemperatureMeasurement = (ResourceObject) CLIM_FACTORY.createWord("TemperatureMeasurement", WordType.RESOURCE);
        CLIM_OceanTemperatureMeasurement = (ResourceObject) CLIM_FACTORY.createWord("OceanTemperatureMeasurement", WordType.RESOURCE);
        CLIM_MaxTemperatureMeasurement = (ResourceObject) CLIM_FACTORY.createWord("MaxTemperatureMeasurement", WordType.RESOURCE);
        CLIM_MinTemperatureMeasurement = (ResourceObject) CLIM_FACTORY.createWord("MinTemperatureMeasurement", WordType.RESOURCE);
        CLIM_PreipitationMeasurement = (ResourceObject) CLIM_FACTORY.createWord("PreipitationMeasurement", WordType.RESOURCE);
        CLIM_RainfallMeasurement = (ResourceObject) CLIM_FACTORY.createWord("RainfallMeasurement", WordType.RESOURCE);
        CLIM_SnowfallMeasurement = (ResourceObject) CLIM_FACTORY.createWord("SnowfallMeasurement", WordType.RESOURCE);
        CLIM_StateRegion = (ResourceObject) CLIM_FACTORY.createWord("StateRegion", WordType.RESOURCE);
        CLIM_TimeTemperatureMeasurement = (ResourceObject) CLIM_FACTORY.createWord("TimeTemperatureMeasurement", WordType.RESOURCE);
        CLIM_Year = (ResourceObject) CLIM_FACTORY.createWord("Year", WordType.RESOURCE);
        
        CLIM_AreaType = (ResourceObject) CLIM_FACTORY.createWord("AreaType", WordType.RESOURCE);

        inicialize_constants = true;
    }

    public static final ResourceObject COUNTRY_TYPE = CLIM_Country;
    public static final ResourceObject INDICATOR_TYPE = CLIM_Indicator;
    public static final ResourceObject DATE_TYPE = CLIM_Date;
    public static final ResourceObject MONTH_TYPE = CLIM_Month;
    public static final ResourceObject YEAR_TYPE = CLIM_Year;
    public static final ResourceObject INDICATOR_DATA_TYPE = CLIM_IndicatorData;
    public static final ResourceObject REGION_TYPE = CLIM_CountryRegion;
    public static final ResourceObject STATION_TYPE = CLIM_Station;
    public static final ResourceObject LOCATION_TYPE = CLIM_Location;
    public static final ResourceObject MEASUREMENT_TYPE = CLIM_Measurement;
    public static final ResourceObject LAND_MEASUREMENT_TYPE = CLIM_LandTemperatureMeasurement;
    public static final ResourceObject OCEAN_MEASUREMENT_TYPE = CLIM_OceanTemperatureMeasurement;

}
