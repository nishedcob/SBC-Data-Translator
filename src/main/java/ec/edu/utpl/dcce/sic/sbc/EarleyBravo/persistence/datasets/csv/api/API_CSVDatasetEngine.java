package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.CSVDatasetEngine;

/**
 * Created by nyx on 6/11/17.
 */
public abstract class API_CSVDatasetEngine extends CSVDatasetEngine {

    public API_CSVDatasetEngine(String dataset_name, String csv_file_path, int number_header_lines) {
        super(Constants.API_DATASET_PREFIX + dataset_name, Constants.API_URL, csv_file_path, number_header_lines);
    }

}
