package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.ersst.v4.data;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.ersst.v4.ERSST_V4_CSVDatasetEngine;

import java.util.List;
import java.util.Scanner;

/**
 * Created by nyx on 6/11/17.
 */
public class ERSST_V4_Year_Data_CSVDatasetEngine extends ERSST_V4_CSVDatasetEngine {

    public ERSST_V4_Year_Data_CSVDatasetEngine() {
        super(Constants.ERSST_YEAR_DATA_DATASET_NAME, Constants.ERSST_V4_YEAR_DATA_DATASET_CSV_PATH,
                Constants.ERSST_DATA_DATASET_NUMBER_HEADER_LINES);
    }

    public SemanticModel readDataset() {
        SemanticModel model = new SemanticModel();

        List<String> rows = this.getInput();

        if (!Constants.isInicialized()) try {
            Constants.inicialize();
        } catch (InvalidWordTypeException e) {
            e.printStackTrace();
            return null;
        }

        int row_num = 1;

        int total_rows = rows.size();

        for (String row : rows) {

            if (Constants.DEBUG) System.out.printf(this.getDataset_name() + ".CSV [ %05d / %05d ]:", row_num, total_rows);

            Scanner rowReader = new Scanner(row);
            rowReader.useDelimiter(Constants.ERSST_DATA_DATASET_DELIMITER);

            if (rowReader.hasNext()) {
                // Year
                String year_string = rowReader.next();
                if (!year_string.equals(Constants.ERSST_V4_DATA_NULL_VALUE) ||
                        !year_string.equals(Constants.ERSST_V4_DATA_ALTERNATE_NULL_VALUE)) {
                    // Create Year
                    InstanceResource year;
                    try {
                        year = new InstanceResource(Constants.CLIM_DATA, String.format(Constants.YEAR_FORMAT,
                                Integer.parseInt(year_string)), Constants.YEAR_TYPE);
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        year = new InstanceResource(Constants.CLIM_DATA, "Year_" + year_string,
                                Constants.YEAR_TYPE);
                    }
                    year.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_year, year_string));
                    model.addResource(year);
                    if (Constants.DEBUG) System.out.printf("Y");

                    if (rowReader.hasNext()) {
                        // Collection Type = Ann
                        rowReader.next();

                        if (rowReader.hasNext()) {
                            // Data Type = { land | ocean | land_ocean }
                            String data_type = rowReader.next();

                            /*if (!data_type.equals(Constants.ERSST_V4_DATA_NULL_VALUE) &&
                                    !data_type.equals(Constants.ERSST_V4_DATA_ALTERNATE_NULL_VALUE)) {
                                year.addPredicateObject(new PredicateLiteralObject(Constants.DCT_title,
                                        data_type));
                                if (Constants.DEBUG) System.out.printf("N");
                            }*/

                            if (rowReader.hasNext()) {
                                // Latitude 1
                                String latitude_string_1 = rowReader.next();

                                /*if (!latitude_string_1.equals(Constants.ERSST_V4_DATA_NULL_VALUE) &&
                                        !latitude_string_1.equals(Constants.ERSST_V4_DATA_ALTERNATE_NULL_VALUE)) {
                                    year.addPredicateObject(new PredicateLiteralObject(Constants.DCT_description,
                                            latitude_string_1));
                                    if (Constants.DEBUG) System.out.printf("D");
                                }*/

                                if (rowReader.hasNext()) {
                                    // Latitude 2
                                    String latitude_string_2 = rowReader.next();

                                    /*if (!latitude_string_2.equals(Constants.ERSST_V4_DATA_NULL_VALUE) &&
                                            !latitude_string_2.equals(Constants.ERSST_V4_DATA_ALTERNATE_NULL_VALUE)) {
                                        year.addPredicateObject(new PredicateLiteralObject(Constants.DCT_source,
                                                latitude_string_2));
                                        if (Constants.DEBUG) System.out.printf("S");
                                    }*/
                                    boolean lat_1, lat_2;
                                    lat_1 = (!latitude_string_1.equals(Constants.ERSST_V4_DATA_NULL_VALUE) &&
                                        !latitude_string_1.equals(Constants.ERSST_V4_DATA_ALTERNATE_NULL_VALUE));
                                    lat_2 = (!latitude_string_2.equals(Constants.ERSST_V4_DATA_NULL_VALUE) &&
                                            !latitude_string_2.equals(Constants.ERSST_V4_DATA_ALTERNATE_NULL_VALUE));
                                    String station_identifier = (
                                            (lat_1 && lat_2) ?
                                                    String.format(Constants.DUAL_LAT_FORMAT,
                                                            latitude_string_1, latitude_string_2) :
                                                    (
                                                            lat_1 ?
                                                                    latitude_string_1 :
                                                                    lat_2 ?
                                                                            latitude_string_2 :
                                                                            Constants.UNKNOWN_STATION
                                                    )
                                    );
                                    String station_string = String.format(Constants.STATION_FORMAT, station_identifier);
                                    InstanceResource station = new InstanceResource(Constants.CLIM_DATA,
                                            station_string, Constants.STATION_TYPE);
                                    if (lat_1) {
                                        String location_string = String.format(Constants.LOCATION_FORMAT,
                                                latitude_string_1);
                                        InstanceResource location = new InstanceResource(Constants.CLIM_DATA,
                                                location_string, Constants.LOCATION_TYPE);
                                        location.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_name,
                                                latitude_string_1));
                                        model.addResource(location);
                                        station.addPredicateObject(new PredicateResourceObject(Constants.CLIM_locatedIn,
                                                location));
                                        if (Constants.DEBUG && lat_1) System.out.printf("L1");
                                    }
                                    if (lat_2) {
                                        String location_string = String.format(Constants.LOCATION_FORMAT,
                                                latitude_string_2);
                                        InstanceResource location = new InstanceResource(Constants.CLIM_DATA,
                                                location_string, Constants.LOCATION_TYPE);
                                        location.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_name,
                                                latitude_string_2));
                                        model.addResource(location);
                                        station.addPredicateObject(new PredicateResourceObject(Constants.CLIM_locatedIn,
                                                location));
                                        if (Constants.DEBUG && lat_2) System.out.printf("L2");
                                    }
                                    model.addResource(station);
                                    if (Constants.DEBUG) System.out.printf("S");

                                    if (rowReader.hasNext()) {

                                        String date_string = String.format(Constants.DATE_FORMAT, year_string);
                                        InstanceResource date = new InstanceResource(
                                                Constants.CLIM_DATA, date_string, Constants.DATE_TYPE
                                        );
                                        date.addPredicateObject(
                                                new PredicateResourceObject(Constants.CLIM_year, year)
                                        );
                                        model.addResource(date);
                                        if (Constants.DEBUG) System.out.printf("D");

                                        String data_point_string = String.format(Constants.DATA_FORMAT,
                                                String.format(
                                                        Constants.DATA_DOUBLE_IDENTIFIER_FORMAT,
                                                        year_string, station_identifier
                                                )
                                        );
                                        InstanceResource data_point = null;
                                        boolean known_data_reading_type = false;

                                        for (String reading_type : Constants.ERSST_V4_DATA_READING_TYPE) {
                                            if (data_type.equals(reading_type)) {

                                                known_data_reading_type = true;
                                                boolean dual_class = false;
                                                ResourceObject type_of_reading = null;

                                                if (
                                                        reading_type.equals(
                                                                Constants.ERSST_V4_DATA_READING_TYPE_LAND
                                                        )
                                                        )
                                                    type_of_reading = Constants.LAND_MEASUREMENT_TYPE;
                                                else if (
                                                        reading_type.equals(
                                                                Constants.ERSST_V4_DATA_READING_TYPE_OCEAN
                                                        )
                                                        )
                                                    type_of_reading = Constants.OCEAN_MEASUREMENT_TYPE;
                                                else dual_class = true;

                                                if (!dual_class && type_of_reading != null)
                                                    data_point = new InstanceResource(
                                                            Constants.CLIM_DATA, data_point_string, type_of_reading
                                                    );
                                                else {
                                                    data_point = new InstanceResource(
                                                            Constants.CLIM_DATA,
                                                            data_point_string,
                                                            Constants.OCEAN_MEASUREMENT_TYPE
                                                    );
                                                    model.addResource(data_point);
                                                    if (Constants.DEBUG) System.out.printf("d");
                                                    data_point = new InstanceResource(
                                                            Constants.CLIM_DATA,
                                                            data_point_string,
                                                            Constants.LAND_MEASUREMENT_TYPE
                                                    );
                                                }
                                                break;
                                            }
                                        }
                                        if (!known_data_reading_type) {
                                            data_point = new InstanceResource(
                                                    Constants.CLIM_DATA, data_point_string, Constants.MEASUREMENT_TYPE
                                            );
                                        }
                                        if (data_point != null) {
                                            data_point.addPredicateObject(
                                                    new PredicateResourceObject(Constants.CLIM_measurementDate, date)
                                            );
                                            data_point.addPredicateObject(
                                                    new PredicateResourceObject(Constants.CLIM_takenAt, station)
                                            );
                                            for (int i = 0; rowReader.hasNext() &&
                                                    i < Constants.ERSST_V4_YEAR_DATA_DATASET_NUMBER_DATAPOINTS; i++) {
                                                String data_string = rowReader.next();
                                                data_point.addPredicateObject(
                                                        new PredicateLiteralObject(
                                                                Constants.CLIM_dataPoint, data_string
                                                        )
                                                );
                                                if (Constants.DEBUG) System.out.printf(".");
                                            }
                                            model.addResource(data_point);
                                            if (Constants.DEBUG) System.out.printf("d");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Constants.DEBUG) {
                row_num++;
                System.out.println();
            }
        }

        return model;
    }

}
