package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic;


import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.PredicateObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Vocabulary;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;
import org.apache.jena.rdf.model.*;

/**
 * Created by nyx on 6/5/17.
 */
public class JenaTranslation {

    public static Model convert(SemanticModel semanticModel) {
        Model model = ModelFactory.createDefaultModel();

        int resource_num = 1;
        int total_resources = semanticModel.getResources().size();

        for (ResourceObject resourceObject : semanticModel.getResources()) {

            if (Constants.DEBUG) System.out.printf("JT [ %05d / %05d ]:", resource_num, total_resources);

            String resourceURI = resourceObject.getVocabulary().getBase_url() + resourceObject.getWord();
            Resource jenaResource = model.createResource(resourceURI);

            if (Constants.DEBUG) System.out.printf(" Po:[ %03d ] ", resourceObject.getPredicateObjects().size());

            for (PredicateObject predicateObject: resourceObject.getPredicateObjects()) {
                String propertyURI = predicateObject.getPredicate().getVocabulary().getBase_url()
                        + predicateObject.getPredicate().getWord();
                Property jenaProperty = model.createProperty(propertyURI);

                if (Constants.DEBUG) System.out.print("P");

                if (predicateObject.getObjectType() == Constants.ObjectType.RESOURCE) {
                    ResourceObject object = ((PredicateResourceObject) predicateObject).getObject();
                    Vocabulary objectVocab = object.getVocabulary();
                    String objectURI = objectVocab.getBase_url() + object.getWord();
                    Resource objectResource = model.createResource(objectURI);
                    jenaResource.addProperty(jenaProperty, objectResource);

                    if (Constants.DEBUG) System.out.print("R");

                } else if (predicateObject.getObjectType() == Constants.ObjectType.LITERAL) {
                    PredicateLiteralObject object = (PredicateLiteralObject) predicateObject;
                    jenaResource.addLiteral(jenaProperty, object.getLiteral());

                    if (Constants.DEBUG) System.out.print("O");

                } else {
                    // Invalid PredicateObject Type
                    // TODO throw new exception
                    return null;
                }
                //jenaResource.addProperty();

            }

            if (Constants.DEBUG) {
                resource_num++;
                System.out.println();
            }

        }
        return model;
    }

    public static String asStatements(Model model) {
        String data = "";
        StmtIterator iterator = model.listStatements();

        int num = 1;

        while (iterator.hasNext()) {

            if (Constants.DEBUG) System.out.printf("JT-AS %05d \n", num);

            Statement statement = iterator.nextStatement();
            Resource  subject = statement.getSubject();
            Property  predicate = statement.getPredicate();
            RDFNode   object = statement.getObject();

            if (!data.equals("")) data += "\n";
            data += String.format("%s %s ", subject.toString(), predicate.toString());
            if (object instanceof Resource) {
                data += String.format("%s .", object.toString());
            } else {
                data += String.format("\"%s\"", object.toString());
            }

            if (Constants.DEBUG) num++;

        }
        return data;
    }

}
