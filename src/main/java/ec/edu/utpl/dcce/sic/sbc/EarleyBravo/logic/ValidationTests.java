package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic;


import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Word;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.ResourceSubclass;

/**
 * Created by nyx on 6/3/17.
 */
public class ValidationTests {

    public static boolean inheritenceTest(Word parent, Word child) {
        if (parent == null) return true;
        // TODO
        if (parent.getWordType() == Constants.WordType.RESOURCE && child.getWordType() == Constants.WordType.RESOURCE) {
            ResourceObject childResource = (ResourceObject) child;
            if (childResource.getResourceType() == Constants.ResourceType.INSTANCE) {
                InstanceResource childInstance = (InstanceResource) childResource;
                if (childInstance.getType() == parent) return true;
                else return inheritenceTest(parent, childInstance.getType());
            } else if (childResource.getResourceType() == Constants.ResourceType.CLASS) {
                ResourceSubclass childSubclass = (ResourceSubclass) childResource;
                if (childSubclass.getSubClassOf() == parent) return true;
                else return inheritenceTest(parent, childSubclass.getSubClassOf());
            } else {
                // unknown type / lack of data integrity
                // TODO throw new exception?
                return false;
            }
        } else if (parent.getWordType() == Constants.WordType.PREDICATE
                && child.getWordType() == Constants.WordType.PREDICATE) {
            // TODO predicate inheritance
            return false;
        } else {
            // incompatible types
            // TODO throw new exception?
            return false;
        }
    }
}
