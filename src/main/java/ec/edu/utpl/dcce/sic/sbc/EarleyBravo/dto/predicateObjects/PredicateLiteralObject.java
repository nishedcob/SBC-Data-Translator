package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Predicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.PredicateObject;

/**
 * Created by nyx on 6/4/17.
 */
public class PredicateLiteralObject extends PredicateObject {

    private String literal;
    private Constants.LiteralDataType dataType;

    public PredicateLiteralObject(Predicate predicate, String literal) {
        super(predicate, Constants.ObjectType.LITERAL);
        this.literal = literal;
    }

    public PredicateLiteralObject(Predicate predicate, String literal, Constants.LiteralDataType dataType) {
        super(predicate, Constants.ObjectType.LITERAL);
        this.literal = literal;
        this.dataType = dataType;
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public Constants.LiteralDataType getDataType() {
        return dataType;
    }

    public void setDataType(Constants.LiteralDataType dataType) {
        this.dataType = dataType;
    }
}
