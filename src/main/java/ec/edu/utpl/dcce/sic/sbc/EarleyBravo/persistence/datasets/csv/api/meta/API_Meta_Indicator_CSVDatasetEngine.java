package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.meta;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic.StringProc;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.API_CSVDatasetEngine;

import java.util.List;
import java.util.Scanner;

/**
 * Created by nyx on 6/11/17.
 */
public class API_Meta_Indicator_CSVDatasetEngine extends API_CSVDatasetEngine {

    public API_Meta_Indicator_CSVDatasetEngine() {
        super(Constants.API_META_INDICATOR_DATASET_NAME, Constants.API_META_INDICATOR_DATASET_CSV_PATH,
                Constants.API_META_INDICATOR_DATASET_NUMBER_HEADER_LINES);
    }

    public SemanticModel readDataset() {
        SemanticModel model = new SemanticModel();

        List<String> rows = this.getInput();

        if (!Constants.isInicialized()) try {
            Constants.inicialize();
        } catch (InvalidWordTypeException e) {
            e.printStackTrace();
            return null;
        }

        int row_num = 2;

        int total_rows = rows.size();

        for (String row : rows) {

            if (Constants.DEBUG) System.out.printf("API_COUNTRY.CSV [ %05d / %05d ]:", row_num, total_rows);

            Scanner rowReader = new Scanner(row);
            rowReader.useDelimiter(Constants.API_META_INDICATOR_DATASET_DELIMITER);

            String indicatorCode = rowReader.next();
            indicatorCode = indicatorCode.replace("\"","");
            if (!indicatorCode.equals("NULL")) {
                InstanceResource indicator = new InstanceResource(Constants.CLIM_DATA, indicatorCode, Constants.INDICATOR_TYPE);
                if (Constants.DEBUG) System.out.print(" I");

                String indicatorName = rowReader.next();
                indicatorName = indicatorName.replace("\"", "");
                if (!indicatorName.equals("NULL")) {
                    indicator.addPredicateObject(new PredicateLiteralObject(Constants.DCT_title, indicatorName));
                    if (Constants.DEBUG) System.out.print(" N");
                }

                String sourceDescription = rowReader.next();
                if (!sourceDescription.equals("NULL")) {
                    indicator.addPredicateObject(new PredicateLiteralObject(Constants.DCT_description, sourceDescription));
                    if (Constants.DEBUG) System.out.print(" S");
                }

                String sourceOrganization = rowReader.next();
                sourceOrganization = sourceOrganization.replace("\"", "");
                if (!sourceOrganization.equals("NULL")) {
                    indicator.addPredicateObject(new PredicateLiteralObject(Constants.DCT_source, sourceOrganization));
                    if (Constants.DEBUG) System.out.print(" O");
                }

                model.addResource(indicator);
            }

            if (Constants.DEBUG) {
                row_num++;
                System.out.println();
            }
        }

        return model;
    }


}
