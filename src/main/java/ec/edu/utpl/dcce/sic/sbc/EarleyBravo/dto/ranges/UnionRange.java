package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ranges;


import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Range;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Word;

import java.util.List;

/**
 * Created by nyx on 6/3/17.
 */
public class UnionRange implements Range {
    private List<Range> ranges;

    public UnionRange(List<Range> ranges) {
        this.ranges = ranges;
    }

    public List<Range> getRanges() {
        return ranges;
    }

    public void setRanges(List<Range> ranges) {
        this.ranges = ranges;
    }

    public void addRange(Range range) {
        if (!ranges.contains(range)) ranges.add(range);
    }

    public void removeRange(Range range) {
        if (ranges.contains(range)) ranges.remove(range);
    }

    public boolean validate(Word word) {
        for (Range r : ranges) {
            if (r.validate(word)) return true;
        }
        return false;
    }
}
