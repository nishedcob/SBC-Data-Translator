package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;

/**
 * Created by nyx on 6/3/17.
 */
public class InvalidWordTypeException extends Exception {

    public InvalidWordTypeException() {
        super(Constants.INVALID_WORD_TYPE_ERROR_MESSAGE);
    }

}
