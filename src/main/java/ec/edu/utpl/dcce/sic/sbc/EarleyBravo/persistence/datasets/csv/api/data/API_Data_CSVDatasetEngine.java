package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.data;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.api.API_CSVDatasetEngine;

import java.util.List;
import java.util.Scanner;

/**
 * Created by nyx on 6/11/17.
 */
public class API_Data_CSVDatasetEngine extends API_CSVDatasetEngine {

    public API_Data_CSVDatasetEngine() {
        super(Constants.API_DATA_DATASET_NAME, Constants.API_DATA_DATASET_CSV_PATH,
                Constants.API_DATA_DATASET_NUMBER_HEADER_LINES);
    }

    public SemanticModel readDataset() {
        SemanticModel model = new SemanticModel();

        List<String> rows = this.getInput();

        if (!Constants.isInicialized()) try {
            Constants.inicialize();
        } catch (InvalidWordTypeException e) {
            e.printStackTrace();
            return null;
        }

        int row_num = 2;

        int total_rows = rows.size();

        for (String row : rows) {

            if (Constants.DEBUG) System.out.printf("API_DATA.CSV [ %05d / %05d ]:", row_num, total_rows);

            Scanner rowReader = new Scanner(row);
            rowReader.useDelimiter(Constants.API_DATA_DATASET_DELIMITER);

            String countryName = rowReader.next();
            InstanceResource country = new InstanceResource(Constants.CLIM_DATA, countryName, Constants.COUNTRY_TYPE);
            model.addResource(country);

            if (Constants.DEBUG) System.out.print("C");

            String indicatorCode = rowReader.next();
            InstanceResource indicator = new InstanceResource(Constants.CLIM_DATA, indicatorCode, Constants.INDICATOR_TYPE);
            model.addResource(indicator);

            if (Constants.DEBUG) System.out.print("I");

            for (int year = Constants.API_DATA_DATASET_MIN_YR; year <= Constants.API_DATA_DATASET_MAX_YR; year++) {
                if (rowReader.hasNext()) {
                    String data = rowReader.next();
                    if (data.equals("NULL")) continue;
                    InstanceResource yearResource = new InstanceResource(Constants.CLIM_DATA, String.format(Constants.YEAR_FORMAT, year), Constants.YEAR_TYPE);
                    model.addResource(yearResource);

                    if (Constants.DEBUG) System.out.print("_");

                    InstanceResource indicatorData = new InstanceResource(Constants.CLIM_DATA, countryName + "_" + indicatorCode + "_" + year, Constants.INDICATOR_DATA_TYPE);
                    indicatorData.addPredicateObject(new PredicateResourceObject(Constants.CLIM_country, country));
                    indicatorData.addPredicateObject(new PredicateResourceObject(Constants.CLIM_year, yearResource));
                    indicatorData.addPredicateObject(new PredicateResourceObject(Constants.CLIM_indicator_Type, indicator));
                    indicatorData.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_dataPoint, data));
                    model.addResource(indicatorData);

                    if (Constants.DEBUG) System.out.print(".");
                }
            }

            if (Constants.DEBUG) {
                row_num++;
                System.out.println();
            }
        }

        return model;
    }


}
