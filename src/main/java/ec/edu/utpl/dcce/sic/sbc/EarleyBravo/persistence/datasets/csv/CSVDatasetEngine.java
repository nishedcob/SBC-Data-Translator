package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.CSVEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.DatasetEngine;

import java.util.List;

/**
 * Created by nyx on 6/11/17.
 */
public abstract class CSVDatasetEngine extends DatasetEngine {

    private List<String> input;

    public CSVDatasetEngine(String dataset_name, String dataset_url, String csv_file_path, int number_header_lines) {
        super(dataset_name + Constants.CSV_ENGINE_DATASET_SUFIX, dataset_url);
        input = CSVEngine.readCSV(csv_file_path, number_header_lines);
    }

    public CSVDatasetEngine(String dataset_name, String csv_file_path, int number_header_lines) {
        super(dataset_name + Constants.CSV_ENGINE_DATASET_SUFIX);
        input = CSVEngine.readCSV(csv_file_path, number_header_lines);
    }

    public List<String> getInput() {
        return input;
    }

    public void setInput(List<String> input) {
        this.input = input;
    }
}
