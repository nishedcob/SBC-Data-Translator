package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.domains;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Domain;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Word;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic.ValidationTests;

/**
 * Created by nyx on 6/3/17.
 */
public class SimpleDomain implements Domain {

    private Word value;

    public SimpleDomain(Word value) {
        this.value = value;
    }

    public Word getValue() {
        return value;
    }

    public void setValue(Word value) {
        this.value = value;
    }

    public boolean validate(Word word) {
        return ValidationTests.inheritenceTest(this.value, word);
    }

}
