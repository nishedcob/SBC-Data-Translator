package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.defwdi.meta;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.defwdi.DEFWDI_CSVDatasetEngine;

import java.util.List;
import java.util.Scanner;

/**
 * Created by nyx on 6/11/17.
 */
public class DEFWDI_Meta_CSVDatasetEngine extends DEFWDI_CSVDatasetEngine {

    public DEFWDI_Meta_CSVDatasetEngine() {
        super(Constants.DEFWDI_META_DATASET_NAME, Constants.DEFWDI_META_DATASET_CSV_PATH,
                Constants.DEFWDI_META_DATASET_NUMBER_HEADER_LINES);
    }

    public SemanticModel readDataset() {
        SemanticModel model = new SemanticModel();

        List<String> rows = this.getInput();

        if (!Constants.isInicialized()) try {
            Constants.inicialize();
        } catch (InvalidWordTypeException e) {
            e.printStackTrace();
            return null;
        }

        int row_num = 1;

        int total_rows = rows.size();

        for (String row : rows) {

            if (Constants.DEBUG) System.out.printf("DEFWDI_META.CSV [ %05d / %05d ]:", row_num, total_rows);

            Scanner rowReader = new Scanner(row);
            rowReader.useDelimiter(Constants.DEFWDI_META_DATASET_DELIMITER);

            if (rowReader.hasNext()) {
                // Indicator Code
                String indicator_code = rowReader.next();
                if (!indicator_code.equals(Constants.DEFWDI_META_NULL_VALUE) ||
                        !indicator_code.equals(Constants.DEFWDI_META_ALTERNATE_NULL_VALUE)) {
                    // Create Indicator
                    InstanceResource indicator = new InstanceResource(Constants.CLIM_DATA, indicator_code,
                            Constants.INDICATOR_TYPE);
                    if (Constants.DEBUG) System.out.printf("I");

                    if (rowReader.hasNext()) {
                        // Indicator Name
                        String indicator_name = rowReader.next();

                        if (!indicator_name.equals(Constants.DEFWDI_META_NULL_VALUE) &&
                                !indicator_name.equals(Constants.DEFWDI_META_ALTERNATE_NULL_VALUE)) {
                            indicator.addPredicateObject(new PredicateLiteralObject(Constants.DCT_title,
                                    indicator_name));
                            if (Constants.DEBUG) System.out.printf("N");
                        }

                        if (rowReader.hasNext()) {
                            // Indicator Long Description
                            String indicator_long_description = rowReader.next();

                            if (!indicator_long_description.equals(Constants.DEFWDI_META_NULL_VALUE) &&
                                    !indicator_long_description.equals(Constants.DEFWDI_META_ALTERNATE_NULL_VALUE)) {
                                indicator.addPredicateObject(new PredicateLiteralObject(Constants.DCT_description,
                                        indicator_long_description));
                                if (Constants.DEBUG) System.out.printf("D");
                            }

                            if (rowReader.hasNext()) {
                                // Indicator Source
                                String indicator_source = rowReader.next();

                                if (!indicator_source.equals(Constants.DEFWDI_META_NULL_VALUE) &&
                                        !indicator_source.equals(Constants.DEFWDI_META_ALTERNATE_NULL_VALUE)) {
                                    indicator.addPredicateObject(new PredicateLiteralObject(Constants.DCT_source,
                                            indicator_source));
                                    if (Constants.DEBUG) System.out.printf("S");
                                }
                            }
                        }
                    }

                    model.addResource(indicator);
                }
            }

            if (Constants.DEBUG) {
                row_num++;
                System.out.println();
            }
        }

        return model;
    }

}
