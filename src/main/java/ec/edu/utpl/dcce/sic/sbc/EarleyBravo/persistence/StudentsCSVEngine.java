package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Vocabulary;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicates.DataPropertyPredicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicates.RelationshipPredicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic.StringProc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by nyx on 6/6/17.
 */
public class StudentsCSVEngine {

    public static SemanticModel readCSV(String file_name) {
        File csv_file = new File(file_name);
        Scanner scanner;
        try {
            scanner = new Scanner(csv_file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        if (scanner.hasNextLine()) scanner.nextLine();
        else return null;
        Vocabulary dct_vocabulary = new Vocabulary("dct", "http://purl.org/dc/terms/");
        DataPropertyPredicate dct_identifier = new DataPropertyPredicate(dct_vocabulary, "identifier");
        Vocabulary teach_vocabulary = new Vocabulary("teach", "http://linkedscience.org/teach/ns#");
        ResourceObject teach_student = new ResourceObject(teach_vocabulary, "Student");
        RelationshipPredicate teach_studentOf = new RelationshipPredicate(teach_vocabulary, "studentOf");
        ResourceObject teach_course = new ResourceObject(teach_vocabulary, "Course");
        Vocabulary foaf_vocabulary = new Vocabulary("foaf", "http://xmlns.com/foaf/");
        DataPropertyPredicate foaf_lastName = new DataPropertyPredicate(foaf_vocabulary, "familyName");
        DataPropertyPredicate foaf_firstName = new DataPropertyPredicate(foaf_vocabulary, "firstName");
        DataPropertyPredicate foaf_givenName = new DataPropertyPredicate(foaf_vocabulary, "givenName");
        DataPropertyPredicate foaf_gender = new DataPropertyPredicate(foaf_vocabulary, "gender");
        Vocabulary dbo_vocabulary = new Vocabulary("dbo", "http://dbpedia.org/ontology/");
        ResourceObject dbo_Province = new ResourceObject(dbo_vocabulary, "Province");
        RelationshipPredicate dbo_province = new RelationshipPredicate(dbo_vocabulary, "province");
        DataPropertyPredicate dbo_status = new DataPropertyPredicate(dbo_vocabulary, "status");
        Vocabulary instances_vocabulary = new Vocabulary("utpl", "http://student.utpl.edu.ec/resource/");
        SemanticModel model = new SemanticModel();
        model.addResource(teach_student);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            Scanner lineScanner = new Scanner(line);
            lineScanner.useDelimiter(",");
            String student_id = lineScanner.next();
            student_id = StringProc.removeQuotes(student_id);
            String student_fullName = lineScanner.next();
            student_fullName = StringProc.removeQuotes(student_fullName);
            String student_lastName = lineScanner.next();
            student_lastName = StringProc.removeQuotes(student_lastName);
            String student_firstName = lineScanner.next();
            student_firstName = StringProc.removeQuotes(student_firstName);
            //String student_givenName = lineScanner.next();
            String student_givenName = student_firstName;
            String student_province = lineScanner.next();
            student_province = StringProc.removeQuotes(student_province);
            String student_course = lineScanner.next();
            student_course = StringProc.removeQuotes(student_course);
            String student_civilStatus = lineScanner.next();
            student_civilStatus = StringProc.removeQuotes(student_civilStatus);
            String student_gender = lineScanner.next();
            student_gender = StringProc.removeQuotes(student_gender);
            String student_resource_name = StringProc.createResourceString("student", student_fullName);
            InstanceResource student = new InstanceResource(instances_vocabulary, student_resource_name, teach_student);
            student.addPredicateObject(new PredicateLiteralObject(dct_identifier, student_id, Constants.LiteralDataType.INTEGER));
            student.addPredicateObject(new PredicateLiteralObject(foaf_lastName, student_lastName, Constants.LiteralDataType.STRING));
            student.addPredicateObject(new PredicateLiteralObject(foaf_firstName, student_firstName, Constants.LiteralDataType.STRING));
            student.addPredicateObject(new PredicateLiteralObject(foaf_givenName, student_givenName, Constants.LiteralDataType.STRING));
            String province_name = StringProc.createResourceString("province", student_province);
            InstanceResource province_student = new InstanceResource(instances_vocabulary, province_name, dbo_Province);
            student.addPredicateObject(new PredicateResourceObject(dbo_province, province_student));
            String course_name = StringProc.createResourceString("course", student_course);
            InstanceResource course_student = new InstanceResource(instances_vocabulary, course_name, teach_course);
            student.addPredicateObject(new PredicateResourceObject(teach_studentOf, course_student));
            student.addPredicateObject(new PredicateLiteralObject(dbo_status, student_civilStatus, Constants.LiteralDataType.STRING));
            student.addPredicateObject(new PredicateLiteralObject(foaf_gender, student_gender, Constants.LiteralDataType.STRING));
            model.addResource(student);
        }
        return model;
    }

}
