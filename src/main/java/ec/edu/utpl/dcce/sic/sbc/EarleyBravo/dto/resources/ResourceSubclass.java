package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Vocabulary;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;

/**
 * Created by nyx on 6/3/17.
 */
public class ResourceSubclass extends ResourceObject {

    private ResourceObject subClassOf;

    public ResourceSubclass(Vocabulary vocabulary, String word, ResourceObject subClassOf) {
        super(vocabulary, word);
        this.subClassOf = subClassOf;
        setResourceType(Constants.ResourceType.CLASS);
        if (!Constants.isInicialized()) try {
            Constants.inicialize();
        } catch (InvalidWordTypeException e) {
            e.printStackTrace();
        }
        setType(Constants.RDFS_CLASS);
    }

    public ResourceObject getSubClassOf() {
        return subClassOf;
    }

    public void setSubClassOf(ResourceObject subClassOf) {
        this.subClassOf = subClassOf;
    }
}
