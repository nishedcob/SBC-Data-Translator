package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto;


import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;

/**
 * Created by nyx on 6/3/17.
 */
public class Predicate extends Word {

    private Domain domain;
    private Range range;

    private Constants.PredicateType type = null;

    public Predicate(Vocabulary vocabulary, String word) {
        super(vocabulary, word, Constants.WordType.PREDICATE);
        this.domain = null;
        this.range = null;
    }

    public Predicate(Vocabulary vocabulary, String word, Domain domain, Range range) {
        super(vocabulary, word, Constants.WordType.PREDICATE);
        this.domain = domain;
        this.range = range;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public void setDomainAndRange(Domain domain, Range range) {
        this.domain = domain;
        this.range = range;
    }

    public Constants.PredicateType getType() {
        return type;
    }

    public void setType(Constants.PredicateType type) {
        this.type = type;
    }
}
