package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto;

/**
 * Created by nyx on 6/3/17.
 */
public interface Range {

    boolean validate(Word word);

}
