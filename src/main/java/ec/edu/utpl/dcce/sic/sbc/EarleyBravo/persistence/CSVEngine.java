package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by nyx on 6/6/17.
 */
public class CSVEngine {

    public static List<String> readCSV(String file_name, int number_header_lines) {
        File csv_file = new File(file_name);
        Scanner scanner;
        try {
            scanner = new Scanner(csv_file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        for (int i = 0; i < number_header_lines; i++) {
            if (scanner.hasNextLine()) scanner.nextLine();
            else {
                scanner.close();
                return null;
            }
        }
        ArrayList<String> rows = new ArrayList<String>();
        while (scanner.hasNextLine()) rows.add(scanner.nextLine());
        scanner.close();
        return rows;
    }

}
