package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicates;


import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Domain;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Predicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Range;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Vocabulary;

/**
 * Created by nyx on 6/3/17.
 */
public class DataPropertyPredicate extends Predicate {

    public DataPropertyPredicate(Vocabulary vocabulary, String word) {
        super(vocabulary, word);
        setType(Constants.PredicateType.DATA_PROPERTY);
    }

    public DataPropertyPredicate(Vocabulary vocabulary, String word, Domain domain, Range range) {
        super(vocabulary, word, domain, range);
        setType(Constants.PredicateType.DATA_PROPERTY);
    }

}
