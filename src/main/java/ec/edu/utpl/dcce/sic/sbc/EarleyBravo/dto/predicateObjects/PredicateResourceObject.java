package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Predicate;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.PredicateObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;

/**
 * Created by nyx on 6/4/17.
 */
public class PredicateResourceObject extends PredicateObject {

    private ResourceObject object;

    public PredicateResourceObject(Predicate predicate, ResourceObject object) {
        super(predicate, Constants.ObjectType.RESOURCE);
        this.object = object;
    }

    public ResourceObject getObject() {
        return object;
    }

    public void setObject(ResourceObject object) {
        this.object = object;
    }

}
