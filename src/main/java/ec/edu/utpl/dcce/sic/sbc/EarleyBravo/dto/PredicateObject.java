package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;

/**
 * Created by nyx on 6/4/17.
 */
public class PredicateObject {

    private Predicate predicate;
    private Constants.ObjectType objectType;

    public PredicateObject(Predicate predicate, Constants.ObjectType objectType) {
        this.predicate = predicate;
        this.objectType = objectType;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate predicate) {
        this.predicate = predicate;
    }

    public Constants.ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(Constants.ObjectType objectType) {
        this.objectType = objectType;
    }

}
