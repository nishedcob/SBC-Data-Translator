package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.defwdi;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.CSVDatasetEngine;

/**
 * Created by nyx on 6/11/17.
 */
public abstract class DEFWDI_CSVDatasetEngine extends CSVDatasetEngine {

    public DEFWDI_CSVDatasetEngine(String dataset_name, String csv_file_path, int number_header_lines) {
        super(Constants.DEFWDI_DATASET_PREFIX + dataset_name, Constants.DEFWDI_URL, csv_file_path, number_header_lines);
    }

}
