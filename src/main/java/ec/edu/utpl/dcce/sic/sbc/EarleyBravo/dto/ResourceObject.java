package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nyx on 6/3/17.
 */
public class ResourceObject extends Word {

    private Constants.ResourceType resourceType = null;

    private List<PredicateObject> predicateObjects;

    private ResourceObject type;

    public ResourceObject(Vocabulary vocabulary, String word) {
        super(vocabulary, word, Constants.WordType.RESOURCE);
        predicateObjects = new ArrayList<PredicateObject>();
    }

    public ResourceObject(Vocabulary vocabulary, String word, ResourceObject type) {
        super(vocabulary, word, Constants.WordType.RESOURCE);
        predicateObjects = new ArrayList<PredicateObject>();
        this.type = type;
    }

    public Constants.ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(Constants.ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public List<PredicateObject> getPredicateObjects() {
        return predicateObjects;
    }

    public ResourceObject getType() {
        return type;
    }

    public void setType(ResourceObject type) {
        this.type = type;
    }

    public void setPredicateObjects(List<PredicateObject> predicateObjects) {
        this.predicateObjects = predicateObjects;
    }

    public void addPredicateObject(PredicateObject predicateObject) {
        predicateObjects.add(predicateObject);
    }

    public void removePredicateObject(PredicateObject predicateObject) {
        predicateObjects.remove(predicateObject);
    }

}
