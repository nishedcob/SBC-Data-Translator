package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence;


import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.*;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;

/**
 * Created by nyx on 6/4/17.
 */
public class NTriplesEngine extends FormatEngine {

    @Override
    public String convert(SemanticModel semanticModel) {
        String data = "";

        int total_resources = semanticModel.getResources().size();
        int resource_num = 1;

        for (ResourceObject resource : semanticModel.getResources()) {

            if (Constants.DEBUG) System.out.printf("NTE [ %05d / %05d ]:", resource_num, total_resources);

            Vocabulary subjectVocab = resource.getVocabulary();
            String subject = subjectVocab.getBase_url() + resource.getWord();
            if (resource.getType() != null) {
                if (!Constants.isInicialized()) try {
                    Constants.inicialize();
                } catch (InvalidWordTypeException e) {
                    e.printStackTrace();
                    return null;
                }
                if (!data.equals("")) data += "\n";
                data += String.format("<%s> <%s> <%s> .", subject, (Constants.RDF.getBase_url() + Constants.RDF_type.getWord()),
                        (resource.getType().getVocabulary().getBase_url() + resource.getType().getWord()));
            }

            if (Constants.DEBUG) System.out.printf(" %03d ", resource.getPredicateObjects().size());

            for (PredicateObject predicateObject : resource.getPredicateObjects()) {
                Predicate predicate = predicateObject.getPredicate();
                Vocabulary predicateVocab = predicate.getVocabulary();
                String predicateString = predicateVocab.getBase_url() + predicate.getWord();

                if (Constants.DEBUG) System.out.print("P");

                String objectString = "";
                String objectFormatString = "";
                if (predicateObject.getObjectType() == Constants.ObjectType.RESOURCE) {
                    PredicateResourceObject objectResource = (PredicateResourceObject) predicateObject;
                    ResourceObject object = objectResource.getObject();
                    Vocabulary objectVocab = object.getVocabulary();
                    String objectName = object.getWord();
                    objectString = objectVocab.getBase_url() + objectName;
                    objectFormatString += "<%s>";

                    if (Constants.DEBUG) System.out.print("R ");

                } else if (predicateObject.getObjectType() == Constants.ObjectType.LITERAL) {
                    PredicateLiteralObject object = (PredicateLiteralObject) predicateObject;
                    objectString = object.getLiteral();
                    objectFormatString += "\"%s\"";

                    if (Constants.DEBUG) System.out.print("L ");

                } else {
                    // Data Integrity Lacking
                    // TODO throw new exeception?
                    return null;
                }
                if (!data.equals("")) data += "\n";
                data += String.format("<%s> <%s> " + objectFormatString + " .", subject, predicateString, objectString);

            }

            if (Constants.DEBUG) {
                resource_num++;
                System.out.println();
            }

        }
        return data;
    }

    @Override
    public SemanticModel parse(String data) {
        // TODO
        return null;
    }
}
