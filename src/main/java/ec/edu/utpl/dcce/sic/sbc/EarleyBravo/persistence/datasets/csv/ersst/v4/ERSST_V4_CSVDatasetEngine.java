package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.ersst.v4;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.CSVDatasetEngine;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.ersst.ERSST_CSVDatasetEngine;

/**
 * Created by nyx on 6/11/17.
 */
public abstract class ERSST_V4_CSVDatasetEngine extends ERSST_CSVDatasetEngine {

    public ERSST_V4_CSVDatasetEngine(String dataset_name, String csv_file_path, int number_header_lines) {
        super(Constants.ERSST_V4_DATASET_PREFIX + dataset_name, csv_file_path, number_header_lines);
    }

}
