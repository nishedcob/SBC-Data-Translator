package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.ersst;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.CSVDatasetEngine;

/**
 * Created by nyx on 6/11/17.
 */
public abstract class ERSST_CSVDatasetEngine extends CSVDatasetEngine {

    public ERSST_CSVDatasetEngine(String dataset_name, String csv_file_path, int number_header_lines) {
        super(dataset_name, Constants.ERSST_URL, csv_file_path, number_header_lines);
    }

}
