package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ranges;


import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Range;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Word;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic.ValidationTests;

/**
 * Created by nyx on 6/3/17.
 */
public class SimpleRange implements Range {

    private Word value;

    public SimpleRange(Word value) {
        this.value = value;
    }

    public Word getValue() {
        return value;
    }

    public void setValue(Word value) {
        this.value = value;
    }

    public boolean validate(Word word) {
        return ValidationTests.inheritenceTest(this.value, word);
    }

}
