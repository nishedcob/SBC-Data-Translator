package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.logic;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by nyx on 6/6/17.
 */
public class StringProc {

    public static String createResourceString(String... strings) {
        String resource_name = "";
        for (String string : strings) {
            string = clean(string);
            if (!resource_name.equals("")) resource_name += "_";
            resource_name += string;
        }
        return resource_name;
    }

    public static String clean(String string) {
        string = removeQuotes(string);
        string = string.replace(" ", "_");
        string = StringUtils.stripAccents(string);
        return string;
    }

    public static String removeQuotes(String string) {
        string = string.replace("\"", "");
        return string;
    }

    public static String abreviate(String words) {
        String abriviation = "";
        for (char c : words.toCharArray()) {
            if (c >= 'A' && c <= 'Z') abriviation += c;
        }
        return abriviation;
    }
}
