package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;

/**
 * Created by nyx on 6/3/17.
 */
public class Word {
    private Vocabulary vocabulary;
    private String word;
    private Constants.WordType wordType;

    public Word(Vocabulary vocabulary, String word, Constants.WordType wordType) {
        this.vocabulary = vocabulary;
        this.word = word;
        this.wordType = wordType;
    }

    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    public void setVocabulary(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Constants.WordType getWordType() {
        return wordType;
    }

    public void setWordType(Constants.WordType wordType) {
        this.wordType = wordType;
    }
}
