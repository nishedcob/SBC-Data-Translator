package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nyx on 6/4/17.
 */
public class SemanticModel {

    private List<ResourceObject> resources;

    public SemanticModel() {
        resources = new ArrayList<ResourceObject>();
    }

    public SemanticModel(List<ResourceObject> resources) {
        this.resources = resources;
    }

    public List<ResourceObject> getResources() {
        return resources;
    }

    public void setResources(List<ResourceObject> resources) {
        this.resources = resources;
    }

    public void addResource(ResourceObject resource) {
        for (ResourceObject resourceObject : resources) {
            if (resource.equals(resourceObject)) return;
        }
        resources.add(resource);
    }

    public void removeResource(ResourceObject resource) {
        resources.remove(resource);
    }

    public SemanticModel concat(SemanticModel model) {
        for (ResourceObject resource : model.getResources()) {
            addResource(resource);
        }
        return this;
    }

    public static SemanticModel concat(SemanticModel model, SemanticModel model2) {
        return model.concat(model2);
    }

}
