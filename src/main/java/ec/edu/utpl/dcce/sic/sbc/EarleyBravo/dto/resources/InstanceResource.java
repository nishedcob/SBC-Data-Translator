package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.Vocabulary;

/**
 * Created by nyx on 6/3/17.
 */
public class InstanceResource extends ResourceObject {

    public InstanceResource(Vocabulary vocabulary, String word, ResourceObject type) {
        super(vocabulary, word, type);
        setResourceType(Constants.ResourceType.INSTANCE);
    }

}
