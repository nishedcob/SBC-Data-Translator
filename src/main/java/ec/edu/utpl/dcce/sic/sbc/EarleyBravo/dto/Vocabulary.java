package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto;

/**
 * Created by nyx on 6/3/17.
 */
public class Vocabulary {
    private String prefix;
    private String base_url;

    public Vocabulary(String prefix, String base_url) {
        this.prefix = prefix;
        this.base_url = base_url;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getBase_url() {
        return base_url;
    }

    public void setBase_url(String base_url) {
        this.base_url = base_url;
    }
}
