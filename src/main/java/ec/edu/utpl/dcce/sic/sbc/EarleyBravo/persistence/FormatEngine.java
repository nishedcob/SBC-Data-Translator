package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;

/**
 * Created by nyx on 6/4/17.
 */
public abstract class FormatEngine {

    public abstract String convert(SemanticModel semanticModel);

    public abstract SemanticModel parse(String data);

}
