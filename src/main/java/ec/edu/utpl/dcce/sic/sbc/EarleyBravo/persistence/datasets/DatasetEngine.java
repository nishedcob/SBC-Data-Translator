package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;

/**
 * Created by nyx on 6/11/17.
 */
public abstract class DatasetEngine {

    private String dataset_name;
    private String dataset_url;

    public abstract SemanticModel readDataset();

    public DatasetEngine(String dataset_name, String dataset_url) {
        this.dataset_name = dataset_name;
        this.dataset_url = dataset_url;
    }

    public DatasetEngine(String dataset_name) {
        this.dataset_name = dataset_name;
    }

    public DatasetEngine() {

    }

    public String getDataset_name() {
        return dataset_name;
    }

    public void setDataset_name(String dataset_name) {
        this.dataset_name = dataset_name;
    }

    public String getDataset_url() {
        return dataset_url;
    }

    public void setDataset_url(String dataset_url) {
        this.dataset_url = dataset_url;
    }

}
