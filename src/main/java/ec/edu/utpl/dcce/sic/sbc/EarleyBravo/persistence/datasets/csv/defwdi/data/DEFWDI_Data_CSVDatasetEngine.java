package ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.defwdi.data;

import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.Constants;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.ResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.SemanticModel;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.exceptions.InvalidWordTypeException;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateLiteralObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.predicateObjects.PredicateResourceObject;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.dto.resources.InstanceResource;
import ec.edu.utpl.dcce.sic.sbc.EarleyBravo.persistence.datasets.csv.defwdi.DEFWDI_CSVDatasetEngine;

import java.util.List;
import java.util.Scanner;

/**
 * Created by nyx on 6/11/17.
 */
public class DEFWDI_Data_CSVDatasetEngine extends DEFWDI_CSVDatasetEngine {

    public DEFWDI_Data_CSVDatasetEngine() {
        super(Constants.DEFWDI_DATA_DATASET_NAME, Constants.DEFWDI_DATA_DATASET_CSV_PATH,
                Constants.DEFWDI_DATA_DATASET_NUMBER_HEADER_LINES);
    }

    public SemanticModel readDataset() {
        SemanticModel model = new SemanticModel();

        List<String> rows = this.getInput();

        if (!Constants.isInicialized()) try {
            Constants.inicialize();
        } catch (InvalidWordTypeException e) {
            e.printStackTrace();
            return null;
        }

        int row_num = 1;

        int total_rows = rows.size();

        for (String row : rows) {

            if (Constants.DEBUG) System.out.printf("DEFWDI_DATA.CSV [ %05d / %05d ]:", row_num, total_rows);

            Scanner rowReader = new Scanner(row);
            rowReader.useDelimiter(Constants.DEFWDI_DATA_DATASET_DELIMITER);

            if (rowReader.hasNext()) {
                // Series Name
                String series_name = rowReader.next();

                if (rowReader.hasNext()) {
                    // Series Code
                    String series_code = rowReader.next();

                    if (!series_code.equals(Constants.DEFWDI_DATA_NULL_VALUE) && !series_code.equals(Constants.DEFWDI_DATA_ALTERNATE_NULL_VALUE)) {
                        // Create Series
                        InstanceResource series = new InstanceResource(Constants.CLIM_DATA, series_code, Constants.INDICATOR_TYPE);
                        if (!series_name.equals(Constants.DEFWDI_DATA_NULL_VALUE) && !series_name.equals(Constants.DEFWDI_DATA_ALTERNATE_NULL_VALUE)) {
                            series.addPredicateObject(new PredicateLiteralObject(Constants.DCT_title, series_name));
                            if (Constants.DEBUG) System.out.printf("S");
                        }
                        model.addResource(series);

                        if (rowReader.hasNext()) {
                            // Country Name
                            String country_name = rowReader.next();

                            if (rowReader.hasNext()) {
                                // Country Code
                                String country_code = rowReader.next();

                                if (!country_code.equals(Constants.DEFWDI_DATA_NULL_VALUE) && !country_code.equals(Constants.DEFWDI_DATA_ALTERNATE_NULL_VALUE)) {
                                    // Create Country
                                    InstanceResource country = new InstanceResource(Constants.CLIM_DATA, country_code, Constants.COUNTRY_TYPE);
                                    if (!country_name.equals(Constants.DEFWDI_DATA_NULL_VALUE)) {
                                        country.addPredicateObject(new PredicateLiteralObject(Constants.DCT_title, country_name));
                                        if (Constants.DEBUG) System.out.printf("C");
                                    }
                                    model.addResource(country);

                                    if (rowReader.hasNext()) {
                                        // 1990
                                        String data_string_1990 = rowReader.next();
                                        if (!data_string_1990.equals(Constants.DEFWDI_DATA_NULL_VALUE)) {
                                            if (!data_string_1990.equals(Constants.DEFWDI_DATA_NULL_VALUE) && !data_string_1990.equals(Constants.DEFWDI_DATA_ALTERNATE_NULL_VALUE)) {
                                                InstanceResource data_resource_1990 = new InstanceResource(Constants.CLIM_DATA, country_code + "_" + series_code + "_" + 1990, Constants.INDICATOR_DATA_TYPE);
                                                data_resource_1990.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_year, String.format("%d", 1990)));
                                                data_resource_1990.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_dataPoint, data_string_1990));
                                                data_resource_1990.addPredicateObject(new PredicateResourceObject(Constants.CLIM_country, country));
                                                data_resource_1990.addPredicateObject(new PredicateResourceObject(Constants.CLIM_indicator_Type, series));
                                                if (Constants.DEBUG) System.out.printf(".");
                                                model.addResource(data_resource_1990);
                                            }
                                        }

                                        if (rowReader.hasNext()) {
                                            // 2001
                                            String data_string_2001 = rowReader.next();
                                            if (!data_string_2001.equals(Constants.DEFWDI_DATA_NULL_VALUE) && !data_string_2001.equals(Constants.DEFWDI_DATA_ALTERNATE_NULL_VALUE)) {
                                                InstanceResource data_resource_2001 = new InstanceResource(Constants.CLIM_DATA, country_code + "_" + series_code + "_" + 2001, Constants.INDICATOR_DATA_TYPE);
                                                data_resource_2001.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_year, String.format("%d", 2001)));
                                                data_resource_2001.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_dataPoint, data_string_2001));
                                                data_resource_2001.addPredicateObject(new PredicateResourceObject(Constants.CLIM_country, country));
                                                data_resource_2001.addPredicateObject(new PredicateResourceObject(Constants.CLIM_indicator_Type, series));
                                                if (Constants.DEBUG) System.out.printf(".");
                                                model.addResource(data_resource_2001);
                                            }

                                            // 2007+ (-> 2017)
                                            for (int year = Constants.DEFWDI_DATA_DATASET_MIN_YR; year <= Constants.DEFWDI_DATA_DATASET_MAX_YR; year++) {
                                                if (rowReader.hasNext()) {
                                                    String data_string = rowReader.next();
                                                    if (!data_string.equals(Constants.DEFWDI_DATA_NULL_VALUE) && !data_string.equals(Constants.DEFWDI_DATA_ALTERNATE_NULL_VALUE)) {
                                                        InstanceResource data_resource = new InstanceResource(Constants.CLIM_DATA, country_code + "_" + series_code + "_" + year, Constants.INDICATOR_DATA_TYPE);
                                                        data_resource.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_year, String.format("%d", year)));
                                                        data_resource.addPredicateObject(new PredicateLiteralObject(Constants.CLIM_dataPoint, data_string));
                                                        data_resource.addPredicateObject(new PredicateResourceObject(Constants.CLIM_country, country));
                                                        data_resource.addPredicateObject(new PredicateResourceObject(Constants.CLIM_indicator_Type, series));
                                                        if (Constants.DEBUG) System.out.printf(".");
                                                        model.addResource(data_resource);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Constants.DEBUG) {
                row_num++;
                System.out.println();
            }
        }

        return model;
    }


}
