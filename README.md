# Climate Project Translators
This project was to designed with the translation of CSV climate data from three datasets into a semantic model based on N-Triples.

## Datasets used:
1. __*NOAA* - Extended Reconstructed Sea Surface Temperature (ERSST) v4__

    [ERSST version 4](data/ersst/v4/) contains ocean surface temperatures on an annual and monthly basis since 1880 up until the present day. It was downloaded from: [ftp://ftp.ncdc.noaa.gov/pub/data/noaaglobaltemp/operational/timeseries/ ](ftp://ftp.ncdc.noaa.gov/pub/data/noaaglobaltemp/operational/timeseries/)

    A full description of the dataset may be found here:
    [https://www.ncdc.noaa.gov/data-access/marineocean-data/extended-reconstructed-sea-surface-temperature-ersst-v4](https://www.ncdc.noaa.gov/data-access/marineocean-data/extended-reconstructed-sea-surface-temperature-ersst-v4)

2. __*WorldBank* - Climate Change Socio-Economic Indicators__

    [WorldBank Climate Change Socio-Economic Indicators](data/api/) contains data with regards to socio-economic and their impact on climate change. It was downloaded from: [http://api.worldbank.org/v2/en/topic/19?downloadformat=csv](http://api.worldbank.org/v2/en/topic/19?downloadformat=csv)

    A full description of the dataset can be found here: [http://data.worldbank.org/topic/climate-change](http://data.worldbank.org/topic/climate-change)

3. __*WorldBank* - World Development Indicators related to Climate Change__

    Publicado por el Banco Mundial, está dataset se trata de relacionar como países se están llevando su desarrollo económico y relacionar el mismo con datos de cambio climático y sus efectos al futuro.

    [WorldBank World Development Indicators related to Climate Change](data/defwdi/) contains data with that relates economic development of countries with climate change data and future consequences. It was downloaded from: [http://databank.worldbank.org/data/AjaxDownload/FileDownloadHandler.ashx?filename=67a61c2b-1798-4972-bb7d-cb703503cbb8.zip&filetype=CSV&language=en&displayfile=Data_Extract_From_World_Development_Indicators.zip](http://databank.worldbank.org/data/AjaxDownload/FileDownloadHandler.ashx?filename=67a61c2b-1798-4972-bb7d-cb703503cbb8.zip&filetype=CSV&language=en&displayfile=Data_Extract_From_World_Development_Indicators.zip)

    A full description of the dataset can be found here: [http://databank.worldbank.org/data/reports.aspx?source=2&Topic=19#](http://databank.worldbank.org/data/reports.aspx?source=2&Topic=19#)

## Code Structure
The translation logic is divided in 4 parts:
- **Constants**: Defines the global configuration parameters that the rest of logic consumes.
- **DTO**: Defines the internal object based memory model.
- **Logic**: Performs business logic operations and links the various layers within the application's architecture.
- **Persistence**: Contains logic for reading in, writing out and manipulating data in files and memory. This layer is where the core of the translation happens as a read CSV is automatically converted to the internal semantic model.

### Constants
```{Java}
// General Flags
public final static boolean DEBUG = true;
public final static boolean TIME_DEBUG = true;
public final static boolean TEST = false;
// Operation Flags
public final static boolean TRANSLATION_PROCESS = false;
public final static boolean UNIFICATION_PROCESS = false;
public final static boolean FORMAT_TRANSLATION_PROCESS = false;
public final static boolean FORMAT_TRANSLATION_MULTIPLE_PROCESS = true;
// API Dataset Flags
public static final boolean PROCESS_API = false;
public static final boolean PROCESS_API_DATA = false;
// DEFWDI Dataset Flags
public static final boolean PROCESS_DEFWDI = false;
public static final boolean PROCESS_DEFWDI_DATA = false;
// ERSST Dataset Flags
public static final boolean PROCESS_ERSST = false;
public static final boolean PROCESS_ERSST_V4 = false;
public static final boolean PROCESS_ERSST_V4_MON = false;
public static final boolean PROCESS_ERSST_V4_ANN = false;
// Unification Flags
public static final boolean UNIFY_API = false;
public static final boolean UNIFY_DEFWDI = false;
public static final boolean UNIFY_ERSST_ANN = false;
public static final boolean UNIFY_ERSST_MON = false;
// Format Translation Flags
public static final boolean FORMAT_TRANSLATION_IN_RDF = false;
public static final boolean FORMAT_TRANSLATION_IN_NT = true;
public static final boolean FORMAT_TRANSLATION_OUT_RDF = true;
public static final boolean FORMAT_TRANSLATION_OUT_NT = false;
// Format Translation Multiple Flags
public static final boolean FORMAT_TRANSLATION_NUMBER_ENUMERATION = true;
public static final boolean FORMAT_TRANSLATION_LETTER_ENUMERATION = false;
public static final boolean FORMAT_TRANSLATION_CAPTIAL_LETTER_ENUMERATION = false;
```

The execution flags as shown above determine the behavior of the execution and which datasets should be processed in addition to other operations that may be performed such as uniting RDF files or converting between distinct RDF serialization formats.

```{Java}
public final static String YEAR_FORMAT = "Year_%d";
public final static String STATION_FORMAT = "Station_%s";
public final static String DUAL_LAT_FORMAT = "%s_%s";
public final static String LOCATION_FORMAT = "Location_%s";
public final static String DATE_FORMAT = "Date_%s";
public final static String MONTH_YEAR_FORMAT = "%s_%s_(Month)";
public final static String DATA_FORMAT = "Data_%s";
public final static String DATA_TRIPLE_IDENTIFIER_FORMAT = "%s_%s_%s";
public final static String DATA_DOUBLE_IDENTIFIER_FORMAT = "%s_%s";

public final static String[] MONTHS_OF_THE_YEAR = {
        "January", "Feburary", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
};

public final static int MIN_MONTH_NUMBER = 0;
public final static int MAX_MONTH_NUMBER = MONTHS_OF_THE_YEAR.length - 1;

public final static String UNKNOWN = "Unknown";
public final static String UNKNOWN_STATION = UNKNOWN;
public final static int NON_EXISTENT_MONTH = -13;
```

Formats and other magic number constants provide consistent logic and order in the generated/translated model.

```{Java}
public enum JenaFileType {
    RDF_XML("rdf", "RDF/XML"), N_TRIPLES("nt", "N-TRIPLES");
    private String ext;
    private String jenaType;

    JenaFileType(String ext, String jenaType) {
        this.ext = ext;
        this.jenaType = jenaType;
    }

    public String getExt() {
        return ext;
    }

    public String getJenaType() {
        return jenaType;
    }
}

public final static JenaFileType RDF_INPUT_FILE_TYPE = (
        FORMAT_TRANSLATION_IN_RDF ?
                JenaFileType.RDF_XML :
                (
                        FORMAT_TRANSLATION_IN_NT ?
                                JenaFileType.N_TRIPLES :
                                null
                )
);
public final static String DIRECTORY_FILE_IN_PATH = "/tmp";
public final static String RDF_FILE_IN_PATH = DIRECTORY_FILE_IN_PATH + "/full_model." + RDF_INPUT_FILE_TYPE.getExt();
public final static String RDF_MULTIPLE_FILE_IN_PATH_PATTERN = DIRECTORY_FILE_IN_PATH + "/full_model." + RDF_INPUT_FILE_TYPE.getExt() + ".%s";
public final static char RDF_MULTIPLE_FILE_INICIAL_ENUMERATION_LETTER = (FORMAT_TRANSLATION_CAPTIAL_LETTER_ENUMERATION ? 'A' : 'a');
public final static char RDF_MULTIPLE_FILE_FINAL_ENUMERATION_LETTER = (FORMAT_TRANSLATION_CAPTIAL_LETTER_ENUMERATION ? 'Z' : 'z');
public final static byte RDF_MULTIPLE_FILE_NUMBER_OF_ENUMERATION_DIGITS = 2;
public final static byte RDF_MULTIPLE_FILE_INICIAL_ENUMERATION_NUMBER = 0;
public final static byte RDF_MULTIPLE_FILE_FINAL_ENUMERATION_NUMBER = 10;

public final static JenaFileType RDF_OUTPUT_FILE_TYPE = (
        FORMAT_TRANSLATION_OUT_RDF ?
                JenaFileType.RDF_XML :
                (
                        FORMAT_TRANSLATION_OUT_NT ?
                                JenaFileType.N_TRIPLES :
                                null
                )
);
public final static String DIRECTORY_FILE_OUT_PATH = "/tmp";
public final static String RDF_FILE_OUT_PATH = DIRECTORY_FILE_OUT_PATH + "/full_model." + RDF_OUTPUT_FILE_TYPE.getExt();
```

The definition of entry and exit file types has been standardized by use with Apache Jena.

```{Java}
public static final String NULL_MODEL_RDF = "NULL";
public static final String API_FULL_MODEL_RDF = DIRECTORY_FILE_IN_PATH + "/full_model_api.rdf";
public static final String DEFWDI_FULL_MODEL_RDF = DIRECTORY_FILE_IN_PATH + "/full_model_defwdi.rdf";
public static final String ERSST_ANN_FULL_MODEL_RDF = DIRECTORY_FILE_IN_PATH + "/full_model_ersst_ann.rdf";
public static final String ERSST_MON_FULL_MODEL_RDF = DIRECTORY_FILE_IN_PATH + "/full_model_ersst_mon.rdf";

public static final String[] RDF_FILE_INPUT_GRAPH_PARTITIONS = {
        ( UNIFY_API ? API_FULL_MODEL_RDF : NULL_MODEL_RDF ),
        ( UNIFY_DEFWDI ? DEFWDI_FULL_MODEL_RDF : NULL_MODEL_RDF ),
        ( UNIFY_ERSST_ANN ? ERSST_ANN_FULL_MODEL_RDF : NULL_MODEL_RDF ),
        ( UNIFY_ERSST_MON ? ERSST_MON_FULL_MODEL_RDF : NULL_MODEL_RDF )
};

public final static String CSV_ENGINE_DATASET_SUFIX = "_CSV";

public final static String API_DATASET_PREFIX = "API_";
public final static String API_URL = "http://api.worldbank.org/v2/en/topic/19?downloadformat=csv";
public final static String API_DATA_DIRECTORY = DIRECTORY_FILE_IN_PATH + "/api";
public final static String API_META_DIRECTORY = API_DATA_DIRECTORY;

public final static String API_DATA_DATASET_NAME = "DATA";
public final static String API_DATA_DATASET_CSV_PATH = API_DATA_DIRECTORY + "/api_data.csv";
public final static int API_DATA_DATASET_NUMBER_HEADER_LINES = 1;
public final static String API_DATA_DATASET_DELIMITER = ",";
public final static int API_DATA_DATASET_MIN_YR = 1960;
public final static int API_DATA_DATASET_MAX_YR = 2016;

public final static String API_META_DATASET_NAME = "META";
public final static int API_META_DATASET_NUMBER_HEADER_LINES = 1;
public final static String API_META_DATASET_DELIMITER = ",";

public final static String API_META_COUNTRY_DATASET_NAME = API_META_DATASET_NAME + "_COUNTRY";
public final static String API_META_COUNTRY_DATASET_CSV_PATH = API_META_DIRECTORY + "/api_country.csv";
public final static int API_META_COUNTRY_DATASET_NUMBER_HEADER_LINES = API_META_DATASET_NUMBER_HEADER_LINES;
public final static String API_META_COUNTRY_DATASET_DELIMITER = API_META_DATASET_DELIMITER;

public final static String API_META_INDICATOR_DATASET_NAME = API_META_DATASET_NAME + "_INDICATOR";
public final static String API_META_INDICATOR_DATASET_CSV_PATH = API_META_DIRECTORY + "/api_indicator.csv";
public final static int API_META_INDICATOR_DATASET_NUMBER_HEADER_LINES = API_META_DATASET_NUMBER_HEADER_LINES;
public final static String API_META_INDICATOR_DATASET_DELIMITER = API_META_DATASET_DELIMITER;

public final static String DEFWDI_DATASET_PREFIX = "DEFWDI_";
public final static String DEFWDI_URL = "";
public final static String DEFWDI_DATA_DIRECTORY = "/tmp/defwdi";
public final static String DEFWDI_META_DIRECTORY = DEFWDI_DATA_DIRECTORY;

public final static String DEFWDI_DATA_DATASET_NAME = "DATA";
public final static String DEFWDI_META_DATASET_NAME = "META";
public final static String DEFWDI_DATA_DATASET_CSV_PATH = DEFWDI_DATA_DIRECTORY + "/defwdi_data.csv";
public final static String DEFWDI_META_DATASET_CSV_PATH = DEFWDI_META_DIRECTORY + "/defwdi_meta.csv";
public final static int DEFWDI_DATA_DATASET_NUMBER_HEADER_LINES = 1;
public final static int DEFWDI_META_DATASET_NUMBER_HEADER_LINES = 1;
public final static String DEFWDI_DATA_DATASET_DELIMITER = ",";
public final static String DEFWDI_META_DATASET_DELIMITER = ",";
public final static int DEFWDI_DATA_DATASET_MIN_YR = 2007;
public final static int DEFWDI_DATA_DATASET_MAX_YR = 2016;
public final static String DEFWDI_NULL_VALUE = "NULL";
public final static String DEFWDI_ALTERNATE_NULL_VALUE = "..";
public final static String DEFWDI_DATA_NULL_VALUE = DEFWDI_NULL_VALUE;
public final static String DEFWDI_DATA_ALTERNATE_NULL_VALUE = DEFWDI_ALTERNATE_NULL_VALUE;
public final static String DEFWDI_META_NULL_VALUE = DEFWDI_NULL_VALUE;
public final static String DEFWDI_META_ALTERNATE_NULL_VALUE = DEFWDI_ALTERNATE_NULL_VALUE;

public final static String ERSST_DATASET_PREFIX = "ERSST_";
public final static String ERSST_V4_DATASET_PREFIX = ERSST_DATASET_PREFIX + "V4_";
public final static String ERSST_URL = "";
public final static String ERSST_DATA_DIRECTORY = "/tmp/ersst";
public final static String ERSST_V4_DATA_DIRECTORY = ERSST_DATA_DIRECTORY + "/v4";

public final static String ERSST_DATA_DATASET_NAME = "DATA";
public final static String ERSST_MONTH_DATA_DATASET_NAME = "MON_" + ERSST_DATA_DATASET_NAME;
public final static String ERSST_YEAR_DATA_DATASET_NAME = "ANN_" + ERSST_DATA_DATASET_NAME;
public final static String ERSST_V4_MONTH_DATA_DATASET_CSV_PATH = ERSST_V4_DATA_DIRECTORY + "/united-mon.csv";
public final static String ERSST_V4_YEAR_DATA_DATASET_CSV_PATH = ERSST_V4_DATA_DIRECTORY + "/united-ann.csv";
public final static int ERSST_V4_MONTH_DATA_DATASET_NUMBER_DATAPOINTS = 7;
public final static int ERSST_V4_YEAR_DATA_DATASET_NUMBER_DATAPOINTS = 5;
public final static String ERSST_V4_DATA_READING_TYPE_LAND = "land";
public final static String ERSST_V4_DATA_READING_TYPE_OCEAN = "ocean";
public final static String ERSST_V4_DATA_READING_TYPE_LAND_OCEAN = "land_ocean";
public final static String[] ERSST_V4_DATA_READING_TYPE = {
        ERSST_V4_DATA_READING_TYPE_LAND, ERSST_V4_DATA_READING_TYPE_OCEAN, ERSST_V4_DATA_READING_TYPE_LAND_OCEAN
};
public final static int ERSST_DATA_DATASET_NUMBER_HEADER_LINES = 0;
public final static String ERSST_DATA_DATASET_DELIMITER = ",";
public final static String ERSST_NULL_VALUE = "NULL";
public final static String ERSST_ALTERNATE_NULL_VALUE = "..";
public final static String ERSST_DATA_NULL_VALUE = ERSST_NULL_VALUE;
public final static String ERSST_DATA_ALTERNATE_NULL_VALUE = ERSST_ALTERNATE_NULL_VALUE;
public final static String ERSST_V4_DATA_NULL_VALUE = ERSST_DATA_NULL_VALUE;
public final static String ERSST_V4_DATA_ALTERNATE_NULL_VALUE = ERSST_DATA_ALTERNATE_NULL_VALUE;
```

Each dataset has been defined in terms of constants to make long term maintenance and translation as easy as possible.

The rest of Constants defines Vocabularies that are used within the Semantic Model.

### DTO
The DTO subsystem defines our own in house meta model for N-Triples that we later translate into a Jena model when it is needed for export or serialization.

### Logic
The Logic subsystem defines [String Operations](src/main/java/ec/edu/utpl/dcce/sic/sbc/EarleyBravo/logic/StringProc.java), [Jena Translation](src/main/java/ec/edu/utpl/dcce/sic/sbc/EarleyBravo/logic/JenaTranslation.java) from our custom meta model, [Validation Tests](src/main/java/ec/edu/utpl/dcce/sic/sbc/EarleyBravo/logic/ValidationTests.java) to ensure the quality of our custom meta model and [Main](src/main/java/ec/edu/utpl/dcce/sic/sbc/EarleyBravo/logic/Main.java) application logic.

### Persistence
The persistence subsystem defines the format of each dataset so that it may be read and translated into a semantic model.

## Code Execution and Data Processing
This project has been written from the ground up to use Maven for dependency and compilation management and Jena for semantic modeling import and export. As such, as long as you have Maven 2+ and a Java JDK 1.8+ installed, all the code should work. All code was written and tested on Debian 8/9 GNU/Linux x64 systems and in order to process some of the larger datasets, it may take several hours (4+) and many GiB of RAM (approximately 4+ GiB for the API dataset for example) during which it is recommended that the machine processing the data be left to its own devices and not be used for any other purpose.

Before execution, the flags defined in [Constants](src/main/java/ec/edu/utpl/dcce/sic/sbc/EarleyBravo/Constants.java) should be defined according to the functionality required during the execution.
