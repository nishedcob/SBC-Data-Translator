# Make Working Folder
mkdir -p /tmp/api

# Copy csv to working folder
cp api_data.csv /tmp/api

# Backup Original Data
cp api_data.csv api_data.orig.csv

# Take 500 Large Sample Size
head -n 500 api_data.orig.csv > api_data.head.csv
cp api_data.head.csv api_data.csv

# Fill Blank Cells with NULL
sed 's/,,/,NULL,/g; s/,,/,NULL,/g; s/,$/,NULL/' api_data.csv > api_data.null_filled.csv
cp api_data.null_filled.csv api_data.csv

